import React from 'react';
import { fireEvent, act } from '@testing-library/react';
import { customRender } from '../custom_renderer';
import '@testing-library/jest-dom/extend-expect';

import BanModal from '../../src/components/BanModal';

import submitBan from '../../src/services/ban';

jest.mock('../../src/services/ban', () => ({
  ...jest.requireActual('../../src/services/ban'),
  __esModule: true,
  default: jest.fn(),
}));

describe('BanModal component', () => {
  it('displays if isOpen is true', () => {
    const { queryByText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen />
    );
    expect(queryByText('Ban User')).not.toBeNull();
  });

  it('does not display if isOpen is false', () => {
    const { queryByText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen={false} />
    );
    expect(queryByText('Ban User')).toBeNull();
  });

  it('displays "hours" in the period dropdown by default', () => {
    const { queryByText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen />
    );
    expect(queryByText('Hours')).not.toBeNull();
  });

  it('retains ban period when ban reason or ban length is changed', async () => {
    const { queryByText, getByTestId, getByLabelText } = customRender(
      <BanModal userId={1} postId={1} submitFn={() => {}} cancelFn={() => {}} isOpen />
    );
    expect(queryByText('Hours').selected).toBeTruthy();
    const modalSelect = getByTestId('modal-select');
    await act(async () => {
      fireEvent.focus(modalSelect);
      fireEvent.keyDown(modalSelect, { key: 'ArrowDown', code: 40 });
      fireEvent.change(modalSelect, { target: { value: 'days' } });
    });

    expect(queryByText('Days').selected).toBeTruthy();

    const banReasonInput = getByLabelText('ban-reason');
    await act(async () => fireEvent.change(banReasonInput, { target: { value: 'ban reason' } }));
    expect(queryByText('Days').selected).toBeTruthy();

    const banLengthInput = getByLabelText('ban-length');
    await act(async () => fireEvent.change(banLengthInput, { target: { value: '2' } }));
    expect(queryByText('Days').selected).toBeTruthy();

    await act(async () =>
      fireEvent(queryByText('Submit'), new MouseEvent('click', { bubbles: true }))
    );
    expect(submitBan).toHaveBeenCalledTimes(1);
    expect(submitBan).toHaveBeenCalledWith({
      userId: 1,
      postId: 1,
      banReason: 'ban reason',
      banLength: 48,
    });
  });
});
