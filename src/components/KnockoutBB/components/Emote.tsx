import React from 'react';
import styled from 'styled-components';
import { loadThemeFromStorage } from '../../../services/theme';
import emotes from '../../../utils/emotesList.json';

export interface EmoteDefinition {
  hidden?: boolean;
  title?: string;
  url: string;
  url_dark?: string;
}

interface EmoteProps {
  name: string;
}

const StyledEmote = styled.img`
  vertical-align: baseline;
`;

const Emote = ({ name }: EmoteProps) => {
  const emote = emotes[name];
  const title = emote.title ?? name;
  const theme = loadThemeFromStorage();
  const url =
    emote.url_dark && (theme === 'dark' || theme === 'classic') ? emote.url_dark : emote.url;

  return <StyledEmote className="emote" src={url} alt={`:${name}:`} title={title} />;
};

export default Emote;
