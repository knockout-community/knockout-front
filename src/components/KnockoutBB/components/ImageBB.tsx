import React from 'react';
import styled from 'styled-components';
import SpoilerButton from './SpoilerButton';
import useSpoiler from './useLimitedSpoiler';

interface StyledImageProps {
  spoiler?: boolean;
  isLink?: boolean;
  isThumbnail?: boolean;
}

export const StyledImage = styled.div<StyledImageProps>`
  position: relative;
  display: inline-block;
  overflow: hidden;

  .image {
    display: inline-block;
    max-width: 100%;
    max-height: 100vh;

    ${(props) => props.spoiler && 'filter: blur(45px);'}
    ${(props) => props.spoiler && 'pointer-events: none;'}
    ${(props) => props.isLink && `border: 1px dotted gray;`}

    ${(props) =>
      props.isThumbnail &&
      `max-width: 300px;
      max-height: 300px;`}
  }
`;

export const DEFAULT_IMAGE_ALT_TEXT = 'Embedded post image';
export const SPOILER_IMAGE_ALT_TEXT = 'Spoilered image';

/**
 * Returns if the url is an image
 *
 * @param url - The url to check
 * @returns If the url is an image
 */
export const isImage = (url: string): boolean => {
  const imageExtensions = ['png', 'jpg', 'jpeg', 'gif', 'webp', 'svg', 'bmp'];
  const rgx = /(?:\.([^.]+))?$/;

  const extension = rgx.exec(url.toLowerCase());

  if (!extension) return false;

  return Boolean(imageExtensions.includes(extension[1]));
};

interface ImageBBProps {
  href: string;
  thumbnail: boolean;
  link: boolean;
  spoiler?: boolean;
  title?: string;
}

const ImageBB = ({ href, thumbnail, link, spoiler = false, title = '' }: ImageBBProps) => {
  const [spoilered, setSpoilered, reveal, limitedUser] = useSpoiler(spoiler);

  const button = spoilered && (
    <SpoilerButton
      reveal={reveal}
      limitedUser={limitedUser}
      setSpoilered={setSpoilered}
      contentName="image"
    />
  );

  let imageAlt = spoilered ? SPOILER_IMAGE_ALT_TEXT : title;
  imageAlt ||= DEFAULT_IMAGE_ALT_TEXT;

  const image = <img className="image" src={href} alt={imageAlt} title={imageAlt} />;

  let styledImage = (
    <StyledImage spoiler={spoilered}>
      {button}
      {image}
    </StyledImage>
  );

  if (link || thumbnail) {
    styledImage = (
      <StyledImage isThumbnail={thumbnail} isLink={link} spoiler={spoilered}>
        {button}
        <a href={href} target="_blank" rel="noopener">
          {image}
        </a>
      </StyledImage>
    );
  }

  return styledImage;
};

export default ImageBB;
