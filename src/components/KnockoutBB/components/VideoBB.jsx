import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import SpoilerButton from './SpoilerButton';
import useSpoiler from './useLimitedSpoiler';

/**
 * A styled video block component.
 *
 * @type {Component}
 */
const StyledVideo = styled.div`
  display: inline-block;
  position: relative;
  overflow: hidden;
  margin: 15px 0;

  #video {
    ${(props) => props.spoiler && 'filter: blur(45px);'}
    ${(props) => props.spoiler && 'pointer-events: none;'}
    max-width: 100%;
    max-height: ${(props) => (props.small ? '300px' : '75vh')};
  }

  ${(props) => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
`;

/*
 * A function to determine whether a URL has a video extension.
 *
 * @param {String} url
 * @return {Boolean}
 */
export const isVideo = (url) => {
  const videoExtensions = ['mp4', 'webm'];
  const rgx = /(?:\.([^.]+))?$/;

  const extension = rgx.exec(url.toLowerCase());

  return Boolean(videoExtensions.includes(extension[1]));
};

const VideoBB = ({ href, small, spoiler }) => {
  const [spoilered, setSpoilered, reveal, limitedUser] = useSpoiler(spoiler);

  return (
    <StyledVideo small={small} spoiler={spoilered}>
      {spoilered && (
        <SpoilerButton
          reveal={reveal}
          limitedUser={limitedUser}
          setSpoilered={setSpoilered}
          contentName="video"
        />
      )}
      <video id="video" controls preload="metadata">
        <source src={href} />
      </video>
    </StyledVideo>
  );
};

VideoBB.propTypes = {
  href: PropTypes.string.isRequired,
  small: PropTypes.bool,
  spoiler: PropTypes.bool,
};

VideoBB.defaultProps = {
  small: false,
  spoiler: false,
};

export default VideoBB;
