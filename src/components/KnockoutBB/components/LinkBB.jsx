/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Tooltip from '../../Tooltip';
import getOpenGraphData from '../../../services/openGraph';
import SmartLink from './SmartLinkBB';

const StyledToggleContainer = styled.div`
  display: inline-block;
`;

export const StyledAnchor = styled.a`
  color: #3facff;
  text-decoration: underline;
  cursor: pointer;

  &:hover {
    filter: brightness(1.3);
  }
`;

const LinkBB = ({ href, isSmart, children }) => {
  const [openGraphData, setOpenGraphData] = useState(null);

  const getOgData = async () => {
    if (!href) {
      return;
    }
    const ogData = await getOpenGraphData(href);

    if (ogData) {
      setOpenGraphData(ogData);
    }
  };

  useEffect(() => {
    if (isSmart) {
      getOgData();
    }
  }, [isSmart]);

  const childrenString = typeof children === 'string' ? children : 'Link';
  const display = children && children.length > 0 ? childrenString : `${href}`;

  let url = href || childrenString;
  try {
    const parsedUrl = new URL(url);
    if (!parsedUrl.protocol) {
      url = `http://${url}`;
    }
  } catch (error) {
    console.error(`Invalid URL: ${url}`);
  }
  if (!url.startsWith('/') && !url.startsWith('mailto:')) {
    // Patchwork solution to prevent links without a protocol just appending to host
    url = url.indexOf('http') === -1 ? `http://${url}` : url; // Could be optimised (new URL?), but it'll do. I'll investigate further -ottergauze
  }

  return isSmart && openGraphData ? (
    <SmartLink linkUrl={href || childrenString} {...openGraphData} />
  ) : (
    <>
      <StyledAnchor href={url} target="_blank" rel="noopener ugc">
        {display}
      </StyledAnchor>

      {isSmart && (
        <StyledToggleContainer>
          <Tooltip text="Attempting to fetch link information...">
            &nbsp;
            <i className="fa-solid fa-cloud-arrow-down" />
          </Tooltip>
        </StyledToggleContainer>
      )}
    </>
  );
};

LinkBB.propTypes = {
  href: PropTypes.string,
  isSmart: PropTypes.bool,
  children: PropTypes.node,
};
LinkBB.defaultProps = {
  href: undefined,
  isSmart: undefined,
  children: undefined,
};

export default LinkBB;
