import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import Modal from '../Modals/Modal';
import ModalSelect from '../Modals/ModalSelect';
import submitBan from '../../services/ban';
import {
  ThemeTextColor,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeVerticalPadding,
  ThemeFontFamily,
} from '../../utils/ThemeNew';
import { FieldLabelSmall, TextField } from '../FormControls';

const BAN_PERIODS = [
  {
    text: 'Hours',
    value: 'hours',
  },
  {
    text: 'Days',
    value: 'days',
  },
  {
    text: 'Months',
    value: 'months',
  },
  {
    text: 'Forever',
    value: 'forever',
  },
];

type BanPeriod = typeof BAN_PERIODS[number]['value'];

const computeBanLength = (length: number, period: BanPeriod): number => {
  let totalLength = length;
  switch (period) {
    case 'days': {
      totalLength *= 24;
      break;
    }
    case 'months': {
      totalLength *= 720;
      break;
    }
    case 'forever': {
      totalLength = 0;
      break;
    }
    default: {
      // noop
    }
  }
  return totalLength;
};

const BanLength = styled.div`
  .ban-length {
    display: block;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeMedium};
    font-family: ${ThemeFontFamily};
    line-height: 1.1;
    border: none;
    width: 100%;
    box-sizing: border-box;
    background: ${ThemeBackgroundLighter};
  }

  .ban-period {
    margin-top: ${ThemeVerticalPadding};
    background: ${ThemeBackgroundLighter};
  }
`;

interface BanModalProps {
  userId: number;
  postId?: number;
  submitFn: () => void;
  cancelFn: () => void;
  isOpen: boolean;
}

const BanModal = ({ userId, postId = undefined, submitFn, cancelFn, isOpen }: BanModalProps) => {
  const [banReason, setBanReason] = useState('');
  const [banLength, setBanLength] = useState(1);
  const [banPeriod, setBanPeriod] = useState<BanPeriod>(BAN_PERIODS[0].value);
  const [submitting, setSubmitting] = useState(false);
  const [submitDisabled, setSubmitDisabled] = useState(true);

  const onSubmit = async () => {
    setSubmitting(true);
    await submitBan({
      userId,
      postId,
      banReason,
      banLength: computeBanLength(banLength, banPeriod),
    });
    setBanReason('');
    setBanLength(1);
    setBanPeriod(BAN_PERIODS[0].value);
    submitFn();
    setSubmitting(false);
  };

  useEffect(() => {
    setSubmitDisabled(banReason.length < 1 || banLength < 1);
  }, [banReason, banLength]);

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Ban User"
      cancelFn={cancelFn}
      submitFn={onSubmit}
      isOpen={isOpen}
      disableSubmit={submitDisabled || submitting}
    >
      <FieldLabelSmall>Ban Reason</FieldLabelSmall>
      <TextField
        aria-label="ban-reason"
        value={banReason}
        maxLength={250}
        onChange={(e) => setBanReason(e.target.value)}
      />
      <FieldLabelSmall>Ban Length</FieldLabelSmall>
      <BanLength>
        {banPeriod !== 'forever' && (
          <input
            aria-label="ban-length"
            className="ban-length"
            type="number"
            min="1"
            value={banLength}
            onChange={(e) => setBanLength(Number(e.target.value))}
          />
        )}
        <ModalSelect
          className="ban-period"
          options={BAN_PERIODS}
          onChange={(e) => setBanPeriod(e.target.value as BanPeriod)}
        />
      </BanLength>
    </Modal>
  );
};

export default BanModal;
