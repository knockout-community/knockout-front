import React, { useEffect, useRef, useState } from 'react';
import { findEmotesWithStart } from '../../../utils/emotes';
import Emote from '../../KnockoutBB/components/Emote';
import { EMOTE_CHAR, getNewEmoteQuery } from '../helpers/emotes';
import { StyledUserSearchPopup } from './UserSearchPopup';

interface EmotePreviewPopupProps {
  closePopup: () => void;
  caretPosition: number;
  content: string;
  onSizeChange: (size: { width: number; height: number }) => void;
  selectEmote: (emote: string) => void;
}

const EmotePreviewPopup = ({
  closePopup,
  caretPosition,
  content,
  onSizeChange,
  selectEmote,
}: EmotePreviewPopupProps) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [foundEmotes, setFoundEmotes] = useState<string[]>([]);
  const query = getNewEmoteQuery(content, caretPosition);

  useEffect(() => {
    if (containerRef.current && onSizeChange) {
      const width = containerRef.current.clientWidth;
      const height = containerRef.current.clientHeight;
      onSizeChange({ width, height });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [foundEmotes]);

  useEffect(() => {
    setFoundEmotes(findEmotesWithStart(query));
  }, [query]);

  if (query.length === 0 || foundEmotes.length === 0) {
    return null;
  }

  return (
    <StyledUserSearchPopup ref={containerRef}>
      {foundEmotes.map((result) => (
        <div
          key={result}
          className="result"
          role="button"
          tabIndex={0}
          onClick={() => {
            selectEmote(result);
            closePopup();
          }}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              selectEmote(result);
              closePopup();
            }
          }}
        >
          <Emote name={result} />
          <code>{`${EMOTE_CHAR}${result}${EMOTE_CHAR}`}</code>
        </div>
      ))}
    </StyledUserSearchPopup>
  );
};

export default EmotePreviewPopup;
