import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import UserAvatar from '../Avatar';
import UserRoleWrapper from '../UserRoleWrapper';
import { formattedUsername } from '../../utils/user';

const StyledMiniUserInfo = styled(Link)`
  display: flex;
  align-items: center;

  .avatar {
    margin-right: 8px;
    height: 30px;
    object-fit: cover;
    width: 30px;
    background: rgba(0, 0, 0, 0.1);
  }

  .username {
    font-weight: bold;
    line-height: normal;
  }
`;

const MiniUserInfo = ({ user, as, defaultAvatar }) => (
  <StyledMiniUserInfo as={as} to={`/user/${user.id}`}>
    {user.avatarUrl && (defaultAvatar || user.avatarUrl !== 'none.webp') && (
      <UserAvatar className="avatar" src={user.avatarUrl} alt={`${user.username}'s Avatar`} />
    )}
    <UserRoleWrapper className="username" user={user}>
      {formattedUsername(user.username)}
    </UserRoleWrapper>
  </StyledMiniUserInfo>
);

MiniUserInfo.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number,
    avatarUrl: PropTypes.string,
    username: PropTypes.string,
  }).isRequired,
  as: PropTypes.string,
  defaultAvatar: PropTypes.bool,
};

MiniUserInfo.defaultProps = {
  as: '',
  defaultAvatar: false,
};

export default MiniUserInfo;
