import React, { useState, ChangeEvent, useMemo } from 'react';
import styled from 'styled-components';

import Tooltip from '../Tooltip';
import InputError from '../InputError';

import { listIcons } from '../../services/iconsNew';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import { Tag } from '../Buttons';
import { roleCheck } from '../UserRoleRestricted';
import { MODERATOR_ROLES } from '../../utils/roleCodes';

interface Icon {
  id: number;
  url: string;
  desc: string;
  category: string;
}

interface IconSelectorProps {
  className: string;
  selectedId?: number;
  handleIconChange: (id: number) => void;
  error?: string;
}

interface ClickableIconProps {
  icon: Icon;
  selected: boolean;
  handleClick: () => void;
  className?: string;
}

interface ClickableCategoryProps {
  category: string;
  selected: boolean;
  handleClick: () => void;
}

const ClickableIcon = ({ icon, selected, handleClick, className = '' }: ClickableIconProps) => (
  <button type="button" onClick={handleClick} title={icon.desc} className={className}>
    <Tooltip text={icon.desc}>
      <img src={icon.url} alt={icon.desc} className={selected ? 'selected' : ''} />
    </Tooltip>
  </button>
);

const ClickableCategory = ({ category, selected, handleClick }: ClickableCategoryProps) => (
  <Tag
    type="button"
    onClick={handleClick}
    title={`${category} category`}
    className={selected ? 'selected' : ''}
  >
    {category}
  </Tag>
);

function filterIcons(icons: Icon[], keyword: string, category: string = ''): number[] {
  if (keyword === '' && category === '') {
    return icons.map((icon) => icon.id);
  }

  const loweredAndParsedKeyword = keyword.toLowerCase().replace(/[^\w\s]|_/g, '');
  const filteredIcons = icons.filter((icon) =>
    icon.desc
      .toLowerCase()
      .replace(/[^\w\s]|_/g, '')
      .includes(loweredAndParsedKeyword)
  );

  if (category === '') {
    return filteredIcons.map((icon) => icon.id);
  }

  return filteredIcons.filter((icon) => icon.category === category).map((icon) => icon.id);
}

function getCategories(icons: Icon[]): string[] {
  return icons
    .map((icon) => icon.category)
    .filter((category, index, categories) => categories.indexOf(category) === index);
}

const IconSelector = ({
  className,
  selectedId = undefined,
  handleIconChange,
  error = '',
}: IconSelectorProps) => {
  const icons = useMemo(() => listIcons(roleCheck(MODERATOR_ROLES)), []);
  const iconIds = useMemo(() => icons.map((icon) => icon.id), [icons]);
  const categories = useMemo(() => getCategories(icons), [icons]);

  const [filteredIconIds, setFilteredIconIds] = useState<number[]>(iconIds);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const [selectedCategory, setSelectedCategory] = useState<string>('');

  const handleSearchChange = useMemo(
    () => (e: ChangeEvent<HTMLInputElement>) => {
      setSearchTerm(e.target.value);
      setFilteredIconIds(filterIcons(icons, e.target.value, selectedCategory));
    },
    [icons, selectedCategory]
  );

  const handleCategoryChange = useMemo(
    () => (category: string) => {
      const parsedCategory = category === selectedCategory ? '' : category;
      setSelectedCategory(parsedCategory);
      setFilteredIconIds(filterIcons(icons, searchTerm, parsedCategory));
    },
    [icons, searchTerm, selectedCategory]
  );

  return (
    <div className={className}>
      <div className="icon-input-title">
        <label htmlFor="icon-search">Icon</label>
        <label aria-label="icon-search">
          <span className="screen-reader-only">Icon Search</span>
          <input
            id="icon-search"
            name="icon-search"
            type="text"
            onChange={handleSearchChange}
            value={searchTerm}
            placeholder="Search icons"
          />
        </label>
      </div>
      <div className={`icon-categories ${selectedCategory !== '' ? 'icon-selected' : ''}`}>
        {categories.map((category) => (
          <ClickableCategory
            key={`category_${category}`}
            category={category}
            selected={selectedCategory === category}
            handleClick={() => handleCategoryChange(category)}
          />
        ))}
      </div>
      <div className="icon-area">
        {icons.map((icon) => (
          <ClickableIcon
            key={`icon_${icon.id}`}
            icon={icon}
            selected={icon.id === selectedId}
            handleClick={() => handleIconChange(icon.id)}
            className={filteredIconIds.includes(icon.id) ? '' : 'hidden'}
          />
        ))}
      </div>
      <InputError error={error} />
    </div>
  );
};

export default styled(IconSelector)`
  margin-top: ${ThemeVerticalPadding};

  label {
    span.screen-reader-only {
      display: none;
    }
  }
  .icon-input-title {
    background: ${ThemeBackgroundLighter};
    padding: 0 ${ThemeHorizontalPadding};
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: ${ThemeFontSizeMedium};

    input {
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: none;
      color: ${ThemeTextColor};
      border: none;
      border-bottom: 2px solid ${ThemeTextColor};
      font-size: ${ThemeFontSizeMedium};
    }
  }

  .icon-categories {
    display: flex;
    flex-wrap: wrap;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundDarker};

    &.icon-selected {
      ${Tag} {
        opacity: 0.4;

        &.selected {
          opacity: 1;
        }
      }
    }
  }

  .icon-area {
    background: ${ThemeBackgroundDarker};
    min-height: 64px;
    max-height: 400px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-content: stretch;
    overflow-x: hidden;
    overflow-y: scroll;

    button {
      border: none;
      background: none;
      padding: 0;
      margin: 0;
      cursor: pointer;

      &:hover {
        background: rgba(255, 255, 255, 0.1);
      }

      &.hidden {
        display: none;
      }
    }

    img {
      height: 48px;
      margin: 10px;
      box-sizing: border-box;
      transition: filter 100ms ease-in-out;
      filter: brightness(0.5);

      @media (max-width: 700px) {
        height: 32px;
        margin: 5px;
      }
    }

    button:hover > img {
      filter: brightness(1.5) !important;
    }

    img.selected {
      filter: brightness(1);
      border: 1px solid white;
    }
  }
`;
