/* eslint-disable import/prefer-default-export */
import { submitPost, updatePost } from '../../services/posts';
import { pushSmartNotification } from '../../utils/notification';

import { clearPostContentsFromStorage } from '../../utils/postEditorAutosave';
import { loadDisplayCountrySettingFromStorage } from '../../utils/postOptionsStorage';

const handlePostError = (error) => {
  if (error?.response?.data?.message) {
    pushSmartNotification({ error: error?.response?.data?.message });
  } else if (error?.response?.data?.error) {
    pushSmartNotification({ error: error?.response?.data?.error });
  } else {
    pushSmartNotification({
      error: `Could not submit post. If all else fails, have you tried logging in again?`,
    });
  }
};

export const handleNewPostSubmit = async (
  content,
  threadId,
  postSubmittedCallback,
  setContent,
  setSubmitting
) => {
  const sendCountryInfo = loadDisplayCountrySettingFromStorage();

  try {
    await submitPost({
      content,
      thread_id: threadId,
      sendCountryInfo,
    });

    clearPostContentsFromStorage(threadId);
    postSubmittedCallback({ goToLatest: true });
    setContent('');
  } catch (error) {
    handlePostError(error);
  }
  setSubmitting(false);
};

export const handleEditPostSubmit = async (
  content,
  threadId,
  postId,
  postSubmittedCallback,
  setSubmitting
) => {
  const sendCountryInfo = loadDisplayCountrySettingFromStorage();

  try {
    await updatePost({
      content,
      id: postId,
      thread_id: threadId,
      sendCountryInfo,
    });

    postSubmittedCallback(true);
  } catch (error) {
    handlePostError(error);
  }
  setSubmitting(false);
};
