import dayjs from 'dayjs';
import React from 'react';
import { Link } from 'react-router-dom';
import relativeTime from 'dayjs/plugin/relativeTime';
import styled from 'styled-components';
import { transparentize } from 'polished';
import { ThreadWithLastPost } from 'knockout-schema';
import { getIcon } from '../../services/icons';
import ratingList from '../../utils/ratingList.json';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeHighlightWeaker,
  ThemeFontSizeLarge,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import UserRoleWrapper from '../UserRoleWrapper';
import Pagination from '../Pagination';
import UserAvatar from '../Avatar';
import ThreadActionButton from '../ThreadActionButton';
import { POSTS_PER_PAGE, unreadPostPage } from '../../utils/postsPerPage';
import { formattedUsername } from '../../utils/user';
import { isDeletedUser } from '../../utils/deletedUser';
import StyleableLogo from '../Header/components/StyleableLogo';
import { MobileMediaQuery } from '../SharedStyles';
import { minimalDateFormat } from '../../utils/dateFormat';
import { Checkbox, CheckboxStates } from '../Checkbox';
import LoggedInOnly from '../LoggedInOnly';

dayjs.extend(relativeTime);

interface StyledThreadItemProps {
  showTopRating?: boolean;
  threadOpacity: string;
  minimal?: boolean;
  pinned: boolean;
  locked: boolean;
  randomLogoColor: string;
}

export const StyledThreadItem = styled.div<StyledThreadItemProps>`
  background: ${ThemeBackgroundDarker};
  margin-bottom: calc(${ThemeVerticalPadding} / 2);
  font-size: ${ThemeFontSizeMedium};
  opacity: ${(props) => props.threadOpacity};
  position: relative;
  min-height: 60px;

  display: grid;
  ${(props) =>
    props.showTopRating
      ? `
      grid-template-columns: 55px 1fr 70px ${props.minimal ? 140 : 310}px;`
      : `
      grid-template-columns: 55px 1fr ${props.minimal ? 140 : 310}px;
  `}

  align-items: center;

  .thread-title {
    ${(props) => props.minimal && `font-size: ${ThemeFontSizeLarge(props)};`}
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    ${(props) => {
      if (props.pinned) {
        return 'font-weight: bold';
      }
      if (props.locked) {
        return `color: ${transparentize(0.5, ThemeTextColor(props))};`;
      }
      return null;
    }}
  }

  .image {
    text-align: center;
    img {
      width: 100%;
    }
  }

  .image,
  .info {
    height: 100%;
    padding: 0 ${ThemeVerticalPadding} 0 ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundLighter};
    box-sizing: border-box;
  }

  .thread-icon {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .thread-icon-inner {
    width: 40px;
    height: 40px;
  }

  .content {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  }

  .locked {
    color: #ffcb00;
  }
  .pinned {
    color: #acff49;
  }
  .deleted {
    color: #ff3535;
  }

  .first-row {
    align-items: center;
  }

  .unread-posts {
    background: ${ThemeHighlightWeaker};
    padding: 5px;
    margin-bottom: 0;
    margin-left: 8px;
    display: inline-block;
    line-height: initial;
    font-size: ${ThemeFontSizeSmall};
    color: white;
    transition: 0.2s;
    white-space: nowrap;

    &:hover {
      opacity: 1;
    }
  }

  .second-row {
    font-size: ${ThemeFontSizeSmall};
    text-overflow: ellipsis;
    padding-top: ${ThemeVerticalPadding};

    .thread-tag {
      padding: 1px 2px;
      background: ${ThemeBackgroundLighter};
    }
  }

  .thread-user {
    margin-right: 6px;
  }

  .viewer-count {
    margin: 0 6px;
  }

  ${MobileMediaQuery} {
    .reading-now-text {
      display: none;
    }
  }

  .checkbox {
    vertical-align: text-top;
  }

  .thread-ratings {
    padding: 0;
    font-size: ${ThemeFontSizeSmall};
    text-align: center;
  }

  .top-rating {
    max-width: 21px;
    height: auto;
    vertical-align: sub;
  }

  .rating-count {
    display: block;
    margin-top: calc(${ThemeVerticalPadding} / 2);
  }

  .info {
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: ${ThemeFontSizeSmall};
  }

  .stats-container {
    display: flex;
    width: 140px;
    height: 88%;
    flex-direction: column;
    justify-content: space-around;

    .stats-container-icon {
      margin-right: 3px;
    }
  }

  .stats-container-minimal {
    display: flex;
    height: 100%;
    align-items: center;
    flex-grow: 1;
    justify-content: space-around;

    .stats-container-item {
      display: flex;
      align-items: center;
    }
    .stats-container-text {
      font-size: ${ThemeFontSizeMedium};
    }

    .stats-container-icon {
      font-size: ${ThemeFontSizeLarge};
      margin-right: 6px;
    }
  }

  .latest-post {
    display: flex;
    height: 88%;
    flex: 1;
    margin: 0 ${ThemeHorizontalPadding};
    justify-content: space-between;
    padding-left: ${ThemeHorizontalPadding};

    .threaditem-user-avatar {
      width: 40px;
      height: auto;
      margin: auto;
      margin-right: 0;
      max-height: 40px;
    }

    .logo-avatar {
      --svg-color: ${(props) => props.randomLogoColor};
    }

    .flipped {
      transform: rotateY(180deg);
    }

    .post-info {
      display: flex;
      width: 115px;
      height: 100%;
      flex-direction: column;
      justify-content: space-around;
      line-height: initial;
      margin-right: ${ThemeHorizontalPadding};
    }

    .post-author {
      display: flex;
      width: 80%;
      white-space: pre;
    }
  }

  &:not(.background-image) {
    z-index: 1;
  }

  @media (max-width: 900px) {
    grid-template-columns: 56px 1fr;

    .thread-icon {
      padding: ${ThemeVerticalPadding} 0;
    }

    .thread-ratings,
    .info {
      display: none;
    }

    .spacer-bead {
      display: none;
    }

    .hide-icon {
      margin: ${ThemeVerticalPadding} 0;
    }

    .thread-item-pagination {
      display: inline;
      margin-top: ${ThemeVerticalPadding};

      .pagination-item:first-child {
        margin-left: 0;
      }

      .pagination-item.small {
        display: inline;
      }

      .pagination-item,
      .pagination-spacer {
        display: inline-block;
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding}!important;
      }
    }
  }
`;

interface ThreadItemBackgroundImageProps {
  backgroundUrl?: string;
  backgroundType?: 'cover' | 'tiled';
}

const ThreadItemBackgroundImage = styled.div<ThreadItemBackgroundImageProps>`
  width: 100%;
  height: 100%;
  display: block;
  position: absolute;
  z-index: -1;
  opacity: 0.075;

  background-image: url(${(props) => props.backgroundUrl});
  ${(props) => props.backgroundType === 'cover' && "background-size: 'cover';"}
  background-position: 'center center';
  background-repeat: ${(props) => (props.backgroundType === 'tiled' ? 'repeat' : 'no-repeat')};
`;

interface ThreadItemProps {
  thread: ThreadWithLastPost;
  showTopRating?: boolean;
  threadOpacity?: string;
  markUnreadAction?: () => void;
  markUnreadText?: string;
  minimal?: boolean;
  includeCheckbox?: boolean;
  checkboxAction?: (action: boolean) => void;
  checked?: boolean;
  hideAction?: () => void;
  isRead?: boolean;
}

const ThreadItem = ({
  thread,
  showTopRating = true,
  threadOpacity = '1.0',
  markUnreadAction = undefined,
  markUnreadText = 'Mark unread',
  minimal = false,
  includeCheckbox = false,
  checkboxAction = undefined,
  checked = false,
  hideAction = undefined,
  isRead = true,
}: ThreadItemProps) => {
  const lastPage = Math.ceil(thread.postCount / POSTS_PER_PAGE);

  const formatDate = (date) => {
    if (!minimal) {
      if (date === 'a few seconds ago') {
        return 'just now';
      }
      return date.replace('ago', 'old');
    }

    return minimalDateFormat(date);
  };

  const icon = getIcon(thread.iconId);
  const threadTag = thread.tags?.length ? Object.values(thread.tags[0])[0] : null;
  const topRating = thread.firstPostTopRating && ratingList[thread.firstPostTopRating.rating];
  const unreadPostId = thread.readThread?.firstUnreadId
    ? `#post-${thread.readThread.firstUnreadId}`
    : '';

  const unreadPostCount = thread.readThread?.unreadPostCount ?? 0;

  const unreadPostPageNum = unreadPostPage(unreadPostCount, thread.postCount);
  const lastPostAvatar = thread.lastPost?.user?.avatarUrl || '';

  const hasAvatar = lastPostAvatar && !lastPostAvatar.includes('none.webp');

  const lastPostDate = thread.lastPost && dayjs(thread.lastPost.createdAt).fromNow();
  const threadDate = formatDate(dayjs(thread.createdAt).fromNow(minimal));

  const threadStatuses = [
    thread.locked && 'Locked',
    thread.pinned && 'Pinned',
    thread.deleted && 'Deleted',
  ]
    .filter((status) => Boolean(status))
    .concat('Thread')
    .join(' | ');

  const deletedUser = isDeletedUser(thread.user?.username);

  const totalViewers = thread.viewers ? thread.viewers.memberCount + thread.viewers.guestCount : 0;

  // if the user has no avatar, display a logo with a random color
  const randomLogoColor = !hasAvatar
    ? Math.floor(Math.sin(thread.lastPost.userId) * 16777215).toString(16)
    : 'ffffff';
  let avatarElement;

  if (!hasAvatar) {
    avatarElement = <StyleableLogo className="threaditem-user-avatar logo-avatar" />;
  } else {
    avatarElement = (
      <UserAvatar
        className="threaditem-user-avatar"
        src={lastPostAvatar}
        alt={`${thread.lastPost?.user?.username}'s avatar`}
      />
    );
  }

  return (
    <StyledThreadItem
      threadOpacity={threadOpacity}
      minimal={minimal}
      showTopRating={showTopRating && thread.firstPostTopRating !== undefined}
      randomLogoColor={`#${randomLogoColor}`}
      locked={thread.locked}
      pinned={thread.pinned}
    >
      <div className="image thread-icon">
        <Link to={`/thread/${thread.id}`}>
          <div className="thread-icon-inner">
            <img src={icon.url} alt={icon.description} />
          </div>
        </Link>
      </div>
      <div className="content">
        <div className="first-row">
          <Link to={`/thread/${thread.id}`} className="thread-title" title={threadStatuses}>
            {thread.locked && (
              <>
                <i className="locked fa-solid fa-lock" />
                &nbsp;
              </>
            )}
            {thread.pinned && (
              <>
                <i className="pinned fa-solid fa-sticky-note" />
                &nbsp;
              </>
            )}
            {thread.deleted && (
              <>
                <i className="deleted fa-solid fa-trash" />
                &nbsp;
              </>
            )}
            {thread.deleted ? '(deleted thread)' : thread.title}
          </Link>
          {isRead && unreadPostCount > 0 ? (
            <Link
              to={`/thread/${thread.id}/${unreadPostPageNum}${unreadPostId}`}
              className="unread-posts"
            >
              {`${unreadPostCount} new ${unreadPostCount === 1 ? 'post' : 'posts'}`}
            </Link>
          ) : null}
        </div>
        {!minimal && (
          <div className="second-row">
            {(thread as any).subforum?.name && (
              <>
                <span className="subforum-name">{(thread as any).subforum?.name}</span>
                <span className="spacer-bead">{' • '}</span>
              </>
            )}
            {threadTag && (
              <>
                <span className="thread-tag">{threadTag}</span>
                <span className="spacer-bead">{' • '}</span>
              </>
            )}
            {deletedUser && (
              <UserRoleWrapper className="thread-user" user={thread.user}>
                {formattedUsername(thread.user.username)}
              </UserRoleWrapper>
            )}
            {!deletedUser && (
              <Link to={`/user/${thread.user.id}`}>
                <UserRoleWrapper className="thread-user" user={thread.user}>
                  {formattedUsername(thread.user.username)}
                </UserRoleWrapper>
              </Link>
            )}
            {thread.postCount && thread.postCount > POSTS_PER_PAGE && (
              <>
                <span className="spacer-bead">•</span>
                <Pagination
                  className="thread-item-pagination"
                  pagePath={`/thread/${thread.id}/`}
                  totalPosts={thread.postCount}
                  small
                />
              </>
            )}
            {totalViewers > 0 && (
              <>
                <span className="spacer-bead">•</span>
                <span className="viewer-count">
                  <i className="fa-solid fa-user-friends viewer-icon" />
                  {` ${totalViewers}`}
                  <span className="reading-now-text">{' reading now'}</span>
                </span>
              </>
            )}
            <LoggedInOnly>
              {hideAction && (
                <>
                  <span className="spacer-bead">•</span>
                  <ThreadActionButton action={hideAction}>
                    <i className="fa-solid fa-eye-slash hide-icon" title="Hide Thread" />
                  </ThreadActionButton>
                </>
              )}
              {thread.readThread && markUnreadAction ? (
                <>
                  <span className="spacer-bead">•</span>
                  <ThreadActionButton action={markUnreadAction}>
                    {markUnreadText}
                  </ThreadActionButton>
                </>
              ) : null}
              {includeCheckbox && checkboxAction && (
                <>
                  <span className="spacer-bead">•</span>
                  <Checkbox
                    state={checked ? CheckboxStates.Checked : CheckboxStates.Unchecked}
                    readOnly={false}
                    onChange={checkboxAction}
                    label=""
                    className="checkbox"
                  />
                </>
              )}
            </LoggedInOnly>
          </div>
        )}
      </div>
      {showTopRating && topRating && (
        <div className="thread-ratings">
          <img
            className="top-rating"
            src={topRating.url}
            title={`Top rating: ${topRating.name}`}
            alt={`Top rating: ${topRating.name}`}
          />
          <span className="rating-count">{thread.firstPostTopRating!.count}</span>
        </div>
      )}
      <Link
        className="info"
        title="Go to latest post"
        to={`/thread/${thread.id}/${lastPage}#post-${thread.lastPost?.id ?? 1}`}
      >
        {!minimal ? (
          <div className="stats-container">
            <span>
              <i className="fa-solid fa-comments stats-container-icon" />
              &nbsp;
              {thread.postCount}
              &nbsp;
              {thread.postCount === 1 ? 'post' : 'posts'}
            </span>
            <span title={thread.createdAt}>
              <i className="fa-solid fa-clock stats-container-icon" />
              &nbsp;
              {threadDate}
            </span>
          </div>
        ) : (
          <div className="stats-container-minimal">
            <div className="stats-container-item">
              <i className="fa-solid fa-comments stats-container-icon" />
              <span className="stats-container-text">{thread.postCount}</span>
            </div>
            <div className="stats-container-item">
              <i className="fa-solid fa-clock stats-container-icon" />
              <span className="stats-container-text">{threadDate}</span>
            </div>
          </div>
        )}
        {!minimal && thread.lastPost?.user && (
          <div className="latest-post">
            <div className="post-info">
              <span>
                <i className="fa-solid fa-angle-double-right" />
                {` ${lastPostDate}`}
              </span>
              <span className="post-author">
                <UserRoleWrapper user={thread.lastPost.user}>
                  {formattedUsername(thread.lastPost.user.username)}
                </UserRoleWrapper>
              </span>
            </div>
            {avatarElement}
          </div>
        )}
      </Link>

      {thread.backgroundUrl && (
        <ThreadItemBackgroundImage
          backgroundUrl={thread.backgroundUrl}
          backgroundType={thread.backgroundType}
        />
      )}
    </StyledThreadItem>
  );
};

export default ThreadItem;
