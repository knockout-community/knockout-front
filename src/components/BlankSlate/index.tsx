import React from 'react';

interface BlankSlateProps {
  resourceNamePlural: string;
}

const BlankSlate = ({ resourceNamePlural }: BlankSlateProps) => {
  return (
    <div>
      <p>{`Sorry, no ${resourceNamePlural} were found.`}</p>
    </div>
  );
};

export default BlankSlate;
