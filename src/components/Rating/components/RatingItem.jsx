import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tooltip from '../../Tooltip';
import ratingList from '../../../utils/ratingList.json';

const StyledRatingItem = styled.div`
  .rating-icon {
    ${(props) => props.ratedByCurrentUser && 'filter: drop-shadow(0px 0px 3px #ffcc00);'}
  }
  ${(props) => props.ratingDisabled && 'button { pointer-events: none; }'}
`;

const RatingItem = ({ rating, count, ratedByCurrentUser, postByCurrentUser, onClick }) => {
  const ratingObject = ratingList[rating];

  if (!ratingObject) {
    return null;
  }

  let tooltipText = `${ratingObject.name}`;
  let suffix = 'Click to rate';
  if (ratedByCurrentUser) {
    suffix = 'Rated by you - Click to unrate';
  } else if (postByCurrentUser) {
    suffix = 'Cannot rate yourself';
  }
  if (onClick !== undefined) {
    tooltipText += ` - ${suffix}`;
  }

  return (
    <StyledRatingItem
      ratingDisabled={postByCurrentUser || onClick === undefined}
      ratedByCurrentUser={ratedByCurrentUser}
      className="rating-bar-item rating-item"
    >
      <Tooltip text={tooltipText} top>
        <div className="rating-icon">
          <button onClick={onClick} type="button" title={tooltipText}>
            <img src={ratingObject.url} alt={ratingObject.name} />
            <span className="count">{`${count}`}</span>
          </button>
        </div>
      </Tooltip>
    </StyledRatingItem>
  );
};

RatingItem.propTypes = {
  rating: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  ratedByCurrentUser: PropTypes.bool.isRequired,
  postByCurrentUser: PropTypes.bool.isRequired,
  onClick: PropTypes.func,
};

RatingItem.defaultProps = {
  onClick: undefined,
};

export default RatingItem;
