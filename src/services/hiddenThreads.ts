import { Thread } from 'knockout-schema';
import { authDelete, authGet, authPost } from './common';

export const hideThread = async (threadId: number) => {
  const results = await authPost({
    url: `/v2/hidden-threads/${threadId}`,
    data: {},
  });
  return results ? results.data.message : null;
};

export const unhideThread = async (threadId: number) => {
  const results = await authDelete({
    url: `/v2/hidden-threads/${threadId}`,
    data: {},
  });
  return results ? results.data.message : null;
};

export const getHiddenThreads = async (): Promise<Thread[]> => {
  const results = await authGet({ url: '/v2/hidden-threads' });
  return results ? results.data : [];
};
