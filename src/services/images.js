import { authPost } from './common';

import { MAX_IMAGE_SIZE_BYTES } from '../utils/limits';

const uploadImage = async ({ imageBlob }) => {
  // check image size before uploading
  if (imageBlob.size > MAX_IMAGE_SIZE_BYTES) {
    return { data: { message: `Image too large (${MAX_IMAGE_SIZE_BYTES / 1000000} MB maximum)` } };
  }

  const formData = new FormData();
  formData.append('image', imageBlob);

  const response = await authPost({
    url: '/postImages',
    data: formData,
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  return response;
};

export default uploadImage;
