import axios from 'axios';

import config from '../../config';

const getOpenGraphData = async (url) => {
  const res = await axios.get(`${config.openGraphHost}/?url=${url}`);

  const { data } = res;

  return data;
};

export default getOpenGraphData;
