import axios from 'axios';
import config from '../../config';

const getRandomThreadAd = async () => {
  try {
    const results = await axios.get(`${config.apiHost}/threadAds/random`);
    return results.data;
  } catch (err) {
    console.error(err);
    return undefined;
  }
};

export default getRandomThreadAd;
