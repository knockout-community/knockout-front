import icons from '../../config/icons';

export function listIcons(
  showAdminIcons: boolean
): { id: number; url: string; desc: string; category: string }[] {
  return icons
    .filter((icon) => {
      if (showAdminIcons) {
        return !icon.disabled;
      }
      return !icon.restricted && !icon.disabled;
    })
    .map((icon) => ({
      id: icon.id,
      url: icon.url,
      desc: icon.description,
      category: icon.category,
    }));
}

export function getIcon(id: number): { id: number; url: string; desc: string } {
  const icon = icons[id] || icons[0];
  return { id, url: icon.url, desc: icon.description };
}
