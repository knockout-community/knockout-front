import { Conversation, Message } from 'knockout-schema';
import { authGet, authPost, authPut } from './common';

interface SendMessageProps {
  receivingUserId: number;
  content: string;
  conversationId?: number;
}

export const getConversations = async (): Promise<Conversation[]> => {
  const results = await authGet({ url: `/conversations` });
  return results.data;
};

export const archiveConversation = async (conversationId: number): Promise<void> => {
  try {
    const result = await authPost({ url: `/conversations/${conversationId}/archive`, data: {} });
    return result?.data;
  } catch (error) {
    throw Error('Unable to archive conversation.');
  }
};

export const sendMessage = async ({
  receivingUserId,
  content,
  conversationId = undefined,
}: SendMessageProps): Promise<Message> => {
  const requestBody = {
    receivingUserId,
    content,
    conversationId,
  };
  const results = await authPost({ url: `/messages`, data: requestBody });
  return results?.data;
};

export const readMessage = async (id: number): Promise<Message> => {
  const results = await authPut({ url: `/messages/${id}`, data: {} });
  if (!results?.data.readAt) {
    throw Error('Unable to mark message as read.');
  }
  return results.data;
};

export const getConversationMessages = async (id: number): Promise<Message[]> => {
  const results = await authGet({ url: `/conversations/${id}` });
  return results.data.messages.reverse();
};
