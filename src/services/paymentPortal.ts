import { PaymentPortal } from 'knockout-schema';
import { authPost } from './common';

export default async (returnUrl: string): Promise<PaymentPortal> => {
  try {
    const result = await authPost({
      url: '/v2/payment-portal',
      data: {
        returnUrl,
      },
    });
    if (result) {
      return result.data;
    }
    return { url: '' };
  } catch (err) {
    console.error(err);
    return { url: '' };
  }
};
