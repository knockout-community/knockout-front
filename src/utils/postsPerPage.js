export const POSTS_PER_PAGE = 20;

export const unreadPostPage = (unreadPostCount, postCount) =>
  Math.ceil((postCount - (unreadPostCount - 1)) / POSTS_PER_PAGE);

export const firstUnreadPage = (lastPostNumber) =>
  Math.ceil(lastPostNumber / POSTS_PER_PAGE) + Number(lastPostNumber % POSTS_PER_PAGE === 0);
