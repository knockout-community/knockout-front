const RATED_PHRASES = [
  'thanks for the rate :^)',
  'you sure about that one?',
  'yea, i can see that',
  'your beenz account has been succesfully charged',
  'this post really deserved it huh',
  'i am the master rater',
  'a good choice of rating',
  'yes, give me more ratings',
  'i judge all ratings on all posts',
  'can you discover the hidden ratings?',
  'thank you',
  'i appreciate you :))',
  'good fortune in your future',
  'i control most things',
  'an appropriate rating',
  'you are a good rater :O)',
];

export default () => {
  return RATED_PHRASES[Math.floor(Math.random() * RATED_PHRASES.length)];
};
