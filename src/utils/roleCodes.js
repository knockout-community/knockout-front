const BANNED_USER = 'banned-user';
const LIMITED_USER = 'limited-user';
const BASIC_USER = 'basic-user';
const GOLD_USER = 'gold-user';
const PAID_GOLD_USER = 'paid-gold-user';
const MODERATOR = 'moderator';
const MODERATOR_IN_TRAINING = 'moderator-in-training';
const ADMIN = 'admin';
const ORANGE_USER = 'orange-user';
const MODERATOR_ROLES = [MODERATOR, MODERATOR_IN_TRAINING, ADMIN];
const GOLD_USER_ROLES = [GOLD_USER, PAID_GOLD_USER].concat(MODERATOR_ROLES);
const REGULAR_USER_ROLES = [LIMITED_USER, BASIC_USER];

module.exports = {
  BANNED_USER,
  LIMITED_USER,
  BASIC_USER,
  GOLD_USER,
  PAID_GOLD_USER,
  ORANGE_USER,
  MODERATOR,
  MODERATOR_IN_TRAINING,
  ADMIN,
  MODERATOR_ROLES,
  GOLD_USER_ROLES,
  REGULAR_USER_ROLES,
};
