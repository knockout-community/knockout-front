/**
 * Retrieves a raw image, if any exist, from the clipboard
 * @param {object} clipboard clipboard data
 * @returns {object} the raw image from the clipboard as a File, or
 * null if no image is found
 */
const getImageFromClipboard = (clipboard) => {
  const { items } = clipboard;

  if (items === undefined) {
    return null;
  }

  let blob = null;

  // clipboard items are not an Array object
  // so we use a good ol' loop here
  for (let i = 0; i < items.length; i += 1) {
    // Skip content if not image
    if (items[i].type.indexOf('image') === 0) {
      blob = items[i].getAsFile();
      break;
    }
  }

  return blob;
};

export default getImageFromClipboard;
