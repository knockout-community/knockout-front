import React, { ReactNode } from 'react';
import EmoteList from './emotesList.json';
import Emote, { EmoteDefinition } from '../components/KnockoutBB/components/Emote';

const EMOTE_REGEX = /:([\w-]{1,20}):/g;

// eslint-disable-next-line import/prefer-default-export
export function replaceTextWithEmotes(text: ReactNode | ReactNode[]) {
  if (Array.isArray(text)) {
    return text.map((node) => replaceTextWithEmotes(node as string));
  }
  if (typeof text !== 'string') {
    return text;
  }

  const nodes: ReactNode[] = [];

  const matches = [...text.matchAll(EMOTE_REGEX)];
  if (matches.length === 0) {
    return text;
  }

  let substringIndex = 0;
  for (const match of matches) {
    // eslint-disable-next-line no-continue
    if (match === undefined || match === null) continue;

    const matchedEmote = EmoteList[match[1]];
    // eslint-disable-next-line no-continue
    if (!matchedEmote) continue;

    const substring = text.substring(substringIndex, match.index);
    if (substring.length > 0) nodes.push(substring);

    substringIndex = (match?.index || 0) + match[0].length;
    nodes.push(<Emote key={`${text}-${substringIndex}`} name={match[1]} />);
  }
  // add dangling newlines or spaces
  if (text.length > substringIndex) nodes.push(text.substring(substringIndex));

  return nodes;
}

export function emoteExists(name: string) {
  return name in EmoteList && !EmoteList[name].hidden;
}

export function findEmotesWithStart(fragment: string) {
  return Object.entries(EmoteList as any as { [key: string]: EmoteDefinition })
    .filter(([key, value]) => !key.startsWith('!') && !value.hidden && key.startsWith(fragment))
    .map(([key]) => key);
}
