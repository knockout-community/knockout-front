import { TextNode } from 'shortcode-tree';
import { ReactElement } from 'react';
import { bbToTree } from '../components/KnockoutBB/Parser';

export interface TagReplacements {
  [tag: string]: ReactElement;
}

const replaceTags = (content: string, tagsToReplace: TagReplacements) => {
  const tree = bbToTree(content);

  const convertNodes = (nodeTree) => {
    if (nodeTree.shortcode === null && nodeTree.children && nodeTree.children.length > 0) {
      return nodeTree.children.map(convertNodes);
    }

    if (nodeTree instanceof TextNode || nodeTree.shortcode === null) {
      return nodeTree.text;
    }

    const convertedNode = tagsToReplace[nodeTree.shortcode.name];

    return convertedNode || nodeTree.shortcode.codeText;
  };

  return convertNodes(tree);
};

export default replaceTags;
