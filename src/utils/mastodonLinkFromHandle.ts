/**
 * Returns a link to a Mastodon user's profile from their handle.
 *
 * @param handle The handle of the Mastodon user
 *
 * @returns The link to the Mastodon user's profile
 *
 * @example
 * mastodonLinkFromHandle('Gargron@knzk.me');
 * // => 'https://knzk.me/@Gargron'
 */
export default (handle?: string): string | null => {
  if (!handle) return null;
  // if the handle doesnt include only one '@', return null
  if (handle.split('@').length !== 2) return null;

  const [user, host] = handle.split('@');
  return `https://${host}/@${user}`;
};
