import { setMessages } from '../state/messages';

const updateMessages = async (id, dispatch, conversations) => {
  const messageState = { messages: [] };
  conversations.forEach((conversation) => {
    if (conversation.messages?.[0].user?.id !== id && conversation.messages?.[0].readAt === null)
      messageState.messages.push(conversation.messages[0]);
  });

  dispatch(setMessages(messageState));
};

export default updateMessages;
