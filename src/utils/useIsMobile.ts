import { useEffect, useState } from 'react';

export default () => {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    const width =
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    setIsMobile(width <= 700);
  }, []);

  return isMobile;
};
