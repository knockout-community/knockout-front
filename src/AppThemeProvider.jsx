import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { ThemeProvider, createGlobalStyle } from 'styled-components';
import { loadScaleFromStorage } from './services/theme';
import { updateTheme } from './state/style';
import { ThemeFontFamily } from './utils/ThemeNew';

const deviceTheme = localStorage.getItem('theme') === 'device';

const GlobalStyles = createGlobalStyle`
  body {
    font-family: ${ThemeFontFamily};
  }
`;

const AppThemeProvider = ({ children }) => {
  const theme = useSelector((state) => state.style.theme);
  const width = useSelector((state) => state.style.width);
  const loggedIn = useSelector((state) => state.user.loggedIn);
  const dispatch = useDispatch();

  useEffect(() => {
    function handleThemeUpdate(event) {
      if (!loggedIn || deviceTheme) {
        const newTheme = event.matches ? 'dark' : 'light';
        dispatch(updateTheme(newTheme));
      }
    }
    window.matchMedia('(prefers-color-scheme: dark)').addListener(handleThemeUpdate);

    if (!loggedIn) {
      const newTheme = window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
      dispatch(updateTheme(newTheme));
    }

    return () =>
      window.matchMedia('(prefers-color-scheme: dark)').removeListener(handleThemeUpdate);
  });

  return (
    <ThemeProvider
      theme={{
        mode: theme,
        width,
        scale: loadScaleFromStorage(),
      }}
    >
      <GlobalStyles />
      {children}
    </ThemeProvider>
  );
};

export default AppThemeProvider;

AppThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
