export const BACKGROUND_UPDATE = 'BACKGROUND_UPDATE';

export function updateBackgroundRequest(value, bgType = 'cover') {
  const bgUrl = value || '/static/bg_dark.jpg';

  return {
    type: BACKGROUND_UPDATE,
    value: bgUrl,
    bgType,
  };
}
