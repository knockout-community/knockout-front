import { User } from 'knockout-schema';

export default (users: User[], currentUser: User) =>
  users.filter((item) => item.id !== currentUser.id)[0];
