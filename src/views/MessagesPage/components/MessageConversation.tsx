import dayjs from 'dayjs';
import React, { useState, useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Link, useHistory, useRouteMatch } from 'react-router-dom';
import { Message, User } from 'knockout-schema';
import { Button, TextButton } from '../../../components/Buttons';
import UserRoleWrapper from '../../../components/UserRoleWrapper';
import { getConversationMessages, readMessage, sendMessage } from '../../../services/messages';
import MessageComponent from './MessageComponent';
import getConversationUser from '../getConversationUser';
import getLighterColor from '../getLighterColor';
import {
  ThemeFontSizeLarge,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { markNotificationAsRead } from '../../../services/notifications';
import { readNotification } from '../../../state/notifications';
import { pushSmartNotification } from '../../../utils/notification';
import { MODERATOR_ROLES } from '../../../utils/roleCodes';

interface StyledMessageConversationProps {
  hasConversation?: boolean;
  submitting?: boolean;
  isMobile?: boolean;
}

const StyledMessageConversation = styled.div<StyledMessageConversationProps>`
  ${(props) => props.hasConversation && `border-left: 1px solid ${getLighterColor(props)};`}
  .messages-header {
    justify-content: flex-start;
    .header-button {
      margin-right: 25px;
    }
  }

  .messages-header.conversation {
    padding-left: 24px;
    padding-right: 24px;
  }

  .disclaimer {
    font-size: ${ThemeFontSizeSmall};
    opacity: 0.4;
    margin-top: 10px;
  }

  .message-input-container {
    position: relative;
  }

  .message-input {
    font-size: ${ThemeFontSizeLarge};
    border-top: 1px solid ${(props) => getLighterColor(props)};
    padding: calc(${ThemeVerticalPadding} * 2) calc(${ThemeHorizontalPadding} * 3);
    line-height: 1.4;
    ${(props) => props.submitting && 'pointer-events: none;'}
    ${(props) => props.submitting && 'opacity: 0.5;'}
    overflow-wrap: break-word;
    ${(props) => props.isMobile && 'padding-right: 50px;'}
  }

  .send-button {
    outline: none;
    border: none;
    background: transparent;
    position: absolute;
    color: ${ThemeTextColor};
    right: 15px;
    top: 15px;
    font-size: 22px;
    opacity: 0.85;
  }

  [contenteditable][placeholder]:empty:before {
    content: attr(placeholder);
    position: absolute;
    opacity: 0.66;
    background-color: transparent;
  }

  .detail-content {
    display: flex;
    flex-direction: column;
    overflow: auto;
  }

  .detail-messages {
    flex-grow: 1;
    padding: 0 calc(${ThemeHorizontalPadding} * 3);
    overflow: auto;
  }

  .detail-messages > :last-child {
    margin-bottom: calc(${ThemeHorizontalPadding} * 4);
  }

  .empty-conversation {
    text-align: center;
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
  }
`;

interface MessageConversationProps {
  getData: () => void;
  currentConversation?: {
    id: number;
    users: User[];
  };
  setModalOpen: (isOpen: boolean) => void;
  backButton?: boolean;
}

const MessageConversation: React.FC<MessageConversationProps> = ({
  getData,
  currentConversation = undefined,
  setModalOpen,
  backButton = false,
}) => {
  const [messages, setMessages] = useState<Message[]>([]);
  const [submitting, setSubmitting] = useState(false);
  const messageList = useRef<HTMLDivElement | null>(null);
  const inputControl = useRef<HTMLDivElement | null>(null);
  const currentUser = useSelector((state: any) => state.user);
  const { notifications } = useSelector((state: any) => state.notifications);
  const history = useHistory();
  const match = useRouteMatch();
  const dispatch = useDispatch();

  const currentConversationUser =
    currentConversation && getConversationUser(currentConversation.users, currentUser);

  useEffect(() => {
    const getMessages = async () => {
      if (currentConversation && currentConversation?.id > 0) {
        const result = await getConversationMessages(currentConversation.id);
        setMessages(result);
      } else {
        setMessages([]);
      }
    };

    getMessages();
    inputControl?.current?.focus();
  }, [currentConversation]);

  useEffect(() => {
    if (messageList.current) {
      messageList.current.scrollTop = messageList.current.scrollHeight;
    }
  }, [messageList, messages]);

  useEffect(() => {
    const existingNotification = notifications[`MESSAGE:${currentConversation?.id}`];
    if (existingNotification && !existingNotification.read) {
      try {
        markNotificationAsRead(existingNotification.id);
        if (currentConversation) {
          dispatch(readNotification(`MESSAGE:${currentConversation.id}`));
        }
      } catch (error) {
        console.error(error);
      }
    }
  }, [notifications, currentConversation]);

  useEffect(() => {
    const lastMessage = messages[messages.length - 1];

    const markAsRead = async () => {
      const result = await readMessage(lastMessage.id);
      setMessages([...messages.slice(0, -1), result]);
      getData();
    };

    if (lastMessage && lastMessage.user.id !== currentUser.id && !lastMessage.readAt) {
      markAsRead();
    }
  }, [messages, currentUser.id]);

  const onInputChange = (e) => {
    if (e.target.innerHTML.trim() === '<br>') {
      e.target.innerHTML = '';
    }
  };

  const pasteAsPlainText = (e) => {
    e.preventDefault();
    const text = (e.originalEvent || e).clipboardData.getData('text');
    document.execCommand('insertHTML', false, text);
  };

  const canSendMessage =
    !currentConversationUser?.disableIncomingMessages ||
    MODERATOR_ROLES.includes(currentUser?.role?.code);

  const handleSubmit = async (e) => {
    try {
      if (e.target.textContent.trim().length && !submitting) {
        setSubmitting(true);
        inputControl?.current?.blur();
        e.target.innerHTML = e.target.innerHTML
          .replaceAll('<div>', '\n')
          .replaceAll('</div>', '')
          .replaceAll('<br>', '\n');
        const content = e.target.textContent.trim();

        const conversationId = currentConversation!.id > 0 ? currentConversation!.id : undefined;
        const result = await sendMessage({
          content,
          conversationId,
          receivingUserId: currentConversationUser!.id,
        });
        if (inputControl?.current?.innerHTML) {
          inputControl.current.innerHTML = '';
        }
        if (!conversationId) {
          await getData();
          history.replace(`/${match.url.split('/')[1]}/${result.conversationId}`);
        } else {
          setMessages((value) => [...value, result]);
          if (messageList.current) {
            messageList.current.scrollTop = messageList.current.scrollHeight;
          }
          if (!backButton) getData();
        }
        inputControl?.current?.focus();
        setSubmitting(false);
      }
    } catch (error) {
      setSubmitting(false);
      pushSmartNotification({
        error: 'Unable to send message. Your account may be too new or have too few posts.',
      });
    }
  };

  const handleKeyDown = (e) => {
    if (e.keyCode === 13 && !e.shiftKey && !backButton) {
      e.preventDefault();
      handleSubmit(e);
    }
  };

  const messageGroups: Message[][] = [];
  let current: Message[] = [];
  messages.forEach((message) => {
    if (
      current.length &&
      (current[current.length - 1].user.id !== message.user.id ||
        dayjs(message.createdAt).diff(current[current.length - 1].createdAt, 'minute') > 10)
    ) {
      messageGroups.push(current);
      current = [];
    }
    current.push(message);
  });
  messageGroups.push(current);

  return (
    <StyledMessageConversation
      submitting={submitting}
      hasConversation={currentConversation !== undefined}
      isMobile={backButton}
      className="column"
    >
      {!currentConversation || !currentConversationUser ? (
        <div className="empty-conversation">
          <div>
            <div className="empty-title">No message selected</div>
            <div className="empty-desc">
              Select one of your existing messages, or create a new one.
            </div>
            <Button onClick={() => setModalOpen(true)}>New message</Button>
          </div>
        </div>
      ) : (
        <>
          <div className="messages-header conversation">
            {backButton && (
              <TextButton
                className="header-button"
                title="Back"
                onClick={() => history.push(`/${match.url.split('/')[1]}`)}
              >
                <i className="header-icon fa-solid fa-arrow-left" />
              </TextButton>
            )}
            <Link to={`/user/${currentConversationUser.id}`}>
              <UserRoleWrapper className="messages-title" user={currentConversationUser}>
                {currentConversationUser.username}
              </UserRoleWrapper>
            </Link>
          </div>
          <div className="messages-body detail-content">
            <div className="detail-messages" ref={messageList}>
              <div className="disclaimer">
                Direct messages are not encrypted; avoid sharing sensitive or personal information.
              </div>
              {messageGroups.map((group, groupIndex) =>
                group.map((message, index) => (
                  <MessageComponent
                    key={message.id}
                    user={message.user}
                    createdAt={message.createdAt}
                    readAt={
                      groupIndex === messageGroups.length - 1 &&
                      index === group.length - 1 &&
                      message.user.id === currentUser.id
                        ? message.readAt
                        : undefined
                    }
                    content={message.content}
                    self={message.user.id === currentUser.id}
                    child={index > 0}
                  />
                ))
              )}
            </div>
            {canSendMessage && (
              <div className="message-input-container">
                <div
                  className="message-input"
                  contentEditable
                  placeholder={`Message ${currentConversationUser.username}`}
                  ref={inputControl}
                  onInput={onInputChange}
                  onPaste={pasteAsPlainText}
                  onKeyDown={handleKeyDown}
                  role="textbox"
                  aria-label="Message text"
                  tabIndex={0}
                />
                {backButton && (
                  <button
                    className="send-button"
                    type="button"
                    onClick={() => handleSubmit({ target: inputControl.current })}
                  >
                    <i className="fa-solid fa-paper-plane" />
                  </button>
                )}
              </div>
            )}
          </div>
        </>
      )}
    </StyledMessageConversation>
  );
};

export default MessageConversation;
