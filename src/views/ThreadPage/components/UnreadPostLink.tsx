import { ThreadWithPosts } from 'knockout-schema';
import React, { useLayoutEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { lighten } from 'polished';
import { firstUnreadPage } from '../../../utils/postsPerPage';
import {
  ThemeFontSizeMedium,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import { TextButton } from '../../../components/Buttons';
import { useAppSelector } from '../../../state/hooks';
import { HEADER_HEIGHT, MOTD_HEIGHT, MOTD_HEIGHT_MOBILE } from '../../../utils/pageScroll';
import { MobileMediaQuery } from '../../../components/SharedStyles';
import Tooltip from '../../../components/Tooltip';
import useIsMobile from '../../../utils/useIsMobile';

interface StyledUnreadPostLinkProps {
  stickyHeader: boolean;
  stickyPosition?: boolean;
  hasMotd: boolean;
}

const StyledUnreadPostLink = styled.div<StyledUnreadPostLinkProps>`
  background: ${ThemeHighlightWeaker};
  margin: 0 ${ThemeHorizontalPadding};
  font-size: ${ThemeFontSizeMedium};
  font-weight: 600;
  display: flex;
  align-items: center;
  color: white;
  ${(props) =>
    props.stickyPosition &&
    `
  position: fixed;
  transform: translateX(-50%);
  left: 50%;
  opacity: 0.94;
  `}
  top: ${(props) => {
    let height = 8;
    if (props.stickyHeader) {
      height += HEADER_HEIGHT + (props.hasMotd ? MOTD_HEIGHT : 0);
    }
    return height;
  }}px;

  ${MobileMediaQuery} {
    top: ${(props) => {
      let height = 8;
      if (props.stickyHeader) {
        height += HEADER_HEIGHT + (props.hasMotd ? MOTD_HEIGHT_MOBILE : 0);
      }
      return height;
    }}px;
    order: 3;
  }
  z-index: 100;

  .unread-post-link {
    flex-grow: 1;
    padding: ${ThemeVerticalPadding} calc(1.5 * ${ThemeHorizontalPadding});
    transition: 0.2s;

    &:hover {
      background: ${(props) => lighten(0.05, ThemeHighlightWeaker(props))};
    }
  }

  .mark-as-read {
    padding: ${ThemeVerticalPadding} calc(1.5 * ${ThemeHorizontalPadding});
    font-size: ${ThemeFontSizeMedium};
    font-weight: 600;
    color: white;
    transition: 0.2s;

    &:hover {
      background: ${(props) => lighten(0.05, ThemeHighlightWeaker(props))};
    }
  }
`;

interface UnreadPostLinkProps {
  thread: ThreadWithPosts;
  stickyPositionEnabled: boolean;
  markThreadRead: () => void;
}

const UnreadPostLink = ({ thread, stickyPositionEnabled, markThreadRead }: UnreadPostLinkProps) => {
  const hasMotd = useAppSelector((state) => state.style.motd);
  const stickyHeader = useAppSelector((state) => state.settings.stickyHeader);
  const [showStickyLink, setShowStickyLink] = useState(false);
  const linkRef = useRef<HTMLDivElement>(null);
  const isMobile = useIsMobile();
  const motdHeight = isMobile ? MOTD_HEIGHT_MOBILE : MOTD_HEIGHT;
  const location = useLocation();
  const showUnreadLink = location.state?.showUnreadLink;

  let stickyHeight = 8;
  if (stickyHeader) {
    stickyHeight += HEADER_HEIGHT + (hasMotd ? motdHeight : 0);
  }

  const shouldShowLink =
    thread.readThread &&
    thread.readThread.unreadPostCount > 0 &&
    ((showUnreadLink && firstUnreadPage(thread.readThread.lastPostNumber) !== thread.currentPage) ||
      firstUnreadPage(thread.readThread.lastPostNumber) < thread.currentPage);

  useLayoutEffect(() => {
    const handleScroll = () => {
      if (shouldShowLink) {
        const { top, bottom } = linkRef.current!.getBoundingClientRect();
        setShowStickyLink(top >= window.innerHeight || bottom <= stickyHeight);
      }
    };

    // Add scroll listener on mount
    window.addEventListener('scroll', handleScroll, { capture: true, passive: true });

    // Remove scroll listener on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll, { capture: true });
    };
  }, [shouldShowLink]);

  return (
    shouldShowLink && (
      <>
        {showStickyLink && stickyPositionEnabled && (
          <StyledUnreadPostLink stickyHeader={stickyHeader} hasMotd={hasMotd} stickyPosition>
            <Link
              to={`/thread/${thread.id}/${firstUnreadPage(
                thread.readThread!.lastPostNumber
              )}#post-${thread.readThread!.firstUnreadId}`}
              className="unread-post-link"
            >
              {thread.readThread!.unreadPostCount}
              &nbsp;unread&nbsp;
              {thread.readThread!.unreadPostCount > 1 ? 'posts' : 'post'}
            </Link>
            <Tooltip text="Mark as read" top={false}>
              <TextButton className="mark-as-read" onClick={markThreadRead}>
                <i className="fa-solid fa-xmark" />
              </TextButton>
            </Tooltip>
          </StyledUnreadPostLink>
        )}
        <StyledUnreadPostLink stickyHeader={stickyHeader} hasMotd={hasMotd} ref={linkRef}>
          <Link
            to={`/thread/${thread.id}/${firstUnreadPage(thread.readThread!.lastPostNumber)}#post-${
              thread.readThread!.firstUnreadId
            }`}
            className="unread-post-link"
          >
            {thread.readThread!.unreadPostCount}
            &nbsp;unread&nbsp;
            {thread.readThread!.unreadPostCount > 1 ? 'posts' : 'post'}
          </Link>
          <Tooltip text="Mark as read">
            <TextButton className="mark-as-read" onClick={markThreadRead}>
              <i className="fa-solid fa-xmark" />
            </TextButton>
          </Tooltip>
        </StyledUnreadPostLink>
      </>
    )
  );
};

export default UnreadPostLink;
