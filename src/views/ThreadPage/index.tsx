import React, { useEffect, useState } from 'react';
import { useHistory, useLocation, useRouteMatch } from 'react-router-dom';

import { Post, ThreadWithPosts } from 'knockout-schema';
import { createAlertRequest, deleteAlertRequest } from '../../services/alerts';
import { createReadThreadRequest } from '../../services/readThreads';
import { getThreadWithPosts, getThread, updateThread } from '../../services/threads';
import { changeThreadStatus } from '../../services/moderation';

import { scrollToBottom, scrollIntoView } from '../../utils/pageScroll';
import { pushNotification, pushSmartNotification } from '../../utils/notification';
import { getSubforumList } from '../../services/subforums';
import { updateBackgroundRequest } from '../../state/background';
import { markNotificationsAsRead } from '../../services/notifications';
import { readNotification } from '../../state/notifications';
import {
  updateSubscriptionThread,
  removeSubscriptionThread,
  addSubscriptionThread,
} from '../../state/subscriptions';

import ThreadPageComponent from './ThreadPage';
import { POSTS_PER_PAGE, firstUnreadPage } from '../../utils/postsPerPage';
import socketClient from '../../socketClient';
import { useAppDispatch, useAppSelector } from '../../state/hooks';

const ThreadPage = () => {
  const dispatch = useAppDispatch();

  const history = useHistory();
  const { hash } = useLocation();
  const match = useRouteMatch<{ id: string; page: string }>();

  const userState = useAppSelector((state) => state.user);
  const notifications = useAppSelector((state) => state.notifications.notifications);
  const { markLastPageRead, autoSubscribe } = useAppSelector((state) => state.settings);

  const [thread, setThread] = useState<ThreadWithPosts>({
    posts: [],
    user: {
      username: '',
    },
    id: undefined,
  } as any);
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [newPostQueue, setNewPostQueue] = useState<Post[]>([]);
  const [moveModal, setMoveModal] = useState({
    show: false,
    options: [] as {
      value: number;
      text: string;
    }[],
  });

  interface PostJumpParams {
    goToLatest: boolean;
    goToPost: boolean;
  }

  const [postJump, setPostJump] = useState<PostJumpParams>({ goToLatest: false, goToPost: false });
  // Not necessarily reflected in thread's existing readThread object
  const [newestSeenReadThreadPost, setNewestSeenReadThreadPost] = useState<number>();

  const updateSubscriptionsMenu = (lastPostNumber: number) => {
    const totalPosts = thread.postCount ?? 1;
    const remainingPosts = totalPosts - lastPostNumber;
    const threadInfo = {
      id: thread.id,
      count: remainingPosts,
      title: thread.title,
      page: firstUnreadPage(lastPostNumber),
      postNum: lastPostNumber,
      iconId: thread.iconId,
      locked: thread.locked,
    };

    // handle updating subscription notification menu
    if (thread.readThread?.isSubscription) {
      dispatch!(
        remainingPosts === 0
          ? removeSubscriptionThread(thread.id)
          : updateSubscriptionThread(threadInfo)
      );
    } else if (remainingPosts > 0) {
      dispatch!(addSubscriptionThread(threadInfo));
    }
  };

  const getLastPostOnPageNumber = (currentPage: number): number => {
    let lastPostNumber = thread.posts[thread.posts.length - 1].threadPostNumber;
    if (!lastPostNumber) {
      // if we cant find the last post number through the post object, calculate it manually
      lastPostNumber = (currentPage - 1) * POSTS_PER_PAGE + thread.posts.length;
    }
    return lastPostNumber;
  };

  const getFirstPostOnPageNumber = (currentPage: number): number => {
    let firstPostNumber = thread.posts[0].threadPostNumber;
    if (!firstPostNumber) {
      // if we cant find the first post number through the post object, calculate it manually
      firstPostNumber = (currentPage - 1) * POSTS_PER_PAGE + 1;
    }
    return firstPostNumber;
  };

  const isLastPostOnPage = (currentPage: number, lastPostNumber: number): boolean =>
    getFirstPostOnPageNumber(currentPage) <= lastPostNumber &&
    lastPostNumber <= getLastPostOnPageNumber(currentPage);

  const isLastPage = (currentPage: number, totalPosts: number): boolean =>
    currentPage === Math.ceil(totalPosts / POSTS_PER_PAGE);

  const updateReadThread = async (subscription = false) => {
    const currentPage = thread.currentPage ?? 1;
    if (thread.posts.length < 1) {
      console.error('No posts found in thread');
      return;
    }

    const lastPostOnPageNumber = getLastPostOnPageNumber(currentPage);
    if (
      isLastPostOnPage(currentPage, (thread.readThread?.lastPostNumber ?? 0) + 1) ||
      (isLastPage(currentPage, thread.postCount) && markLastPageRead)
    ) {
      if (subscription) {
        await createAlertRequest(thread.id, lastPostOnPageNumber);
        updateSubscriptionsMenu(lastPostOnPageNumber);
      } else {
        await createReadThreadRequest(thread.id, lastPostOnPageNumber);
        setNewestSeenReadThreadPost(lastPostOnPageNumber);
      }
    }
  };

  const createAlert = async (
    showNotification = false,
    lastPostNumber = getLastPostOnPageNumber(thread.currentPage)
  ) => {
    try {
      await createAlertRequest(thread.id, lastPostNumber);
      updateSubscriptionsMenu(lastPostNumber);

      socketClient.emit('subscribedThreadPosts:join', thread.id);
      if (showNotification) {
        pushNotification({ message: 'Success! You are now subscribed.', type: 'success' });
      }
      setIsSubscribed(true);
    } catch (error) {
      pushNotification({
        message: 'Oops, we could not subscribe you to this thread. Sorry.',
        type: 'warning',
      });
    }
  };

  const addNewPosts = () => {
    // remove posts from the new post queue that are already in the thread
    const postIdSet = new Set(thread.posts.map((post) => post.id));
    const unfetchedPosts = newPostQueue.filter((newPost) => !postIdSet.has(newPost.id));

    setThread((prevThread) => {
      return {
        ...prevThread,
        posts: [...prevThread.posts, ...unfetchedPosts],
      };
    });
    setNewPostQueue([]);
  };

  /**
   * @param object single object parameter, contains:
   * @param goToLatest boolean
   * @param goToPost boolean
   */
  const jumpToPost = ({ goToLatest, goToPost }) => {
    if (goToLatest) {
      scrollToBottom(1);
    } else if (goToPost) {
      // scroll to anchor in the url
      scrollIntoView(hash);
    }
  };

  const refreshPosts = async ({
    threadId = match.params.id,
    page = match.params.page,
    goToLatest = false,
    goToPost = false,
  } = {}) => {
    try {
      // this will be used to decide between default param or last page
      let targetPage = Number(page) || 1;

      // get the number of the last page
      if (goToLatest) {
        const pureThread = await getThread(threadId);
        const { totalPosts } = pureThread;
        const totalPages = Math.ceil(totalPosts / POSTS_PER_PAGE);
        history.push(`/thread/${threadId}/${totalPages}`);
        targetPage = totalPages;
      }

      // common thread updating logic
      const threadWithPosts: ThreadWithPosts = await getThreadWithPosts(threadId, targetPage);
      // sets state with the new thread data
      setThread(threadWithPosts);
      setNewestSeenReadThreadPost(threadWithPosts.readThread?.lastPostNumber);
      setIsSubscribed(threadWithPosts.readThread?.isSubscription ?? false);
      setPostJump({ goToLatest, goToPost });
    } catch (error) {
      console.error(error);
    }
  };

  const deleteAlert = async (threadId) => {
    try {
      await deleteAlertRequest({ threadId });
      socketClient.emit('subscribedThreadPosts:leave', threadId);
      pushNotification({ message: 'No longer subscribed to thread.', type: 'success' });
      dispatch(removeSubscriptionThread(threadId));
      setIsSubscribed(false);
    } catch (e) {
      console.error(e);
    }
  };

  const updateThreadStatus = async (status) => {
    try {
      const actions: {
        unlock?: boolean;
        lock?: boolean;
        restore?: boolean;
        delete?: boolean;
        unpin?: boolean;
        pinned?: boolean;
      } = {};

      if (status.locked) {
        actions.unlock = true;
      }
      if (status.locked === false) {
        actions.lock = true;
      }
      if (status.deleted) {
        actions.restore = true;
      }
      if (status.deleted === false) {
        actions.delete = true;
      }
      if (status.pinned) {
        actions.unpin = true;
      }
      if (status.pinned === false) {
        actions.pinned = true;
      }

      await changeThreadStatus({ threadId: thread.id, statuses: actions });
      await refreshPosts();
    } catch (err) {
      pushNotification({ message: 'Error!', type: 'error' });
    }
  };

  const showMoveModal = async () => {
    const subforums = await getSubforumList();
    const subforumOptions = subforums.map((subforum) => ({
      value: subforum.id,
      text: subforum.name,
    }));

    setMoveModal({ show: true, options: subforumOptions });
  };

  const submitThreadMove = async (id) => {
    if (!thread.id) {
      pushSmartNotification({ error: 'Invalid thread.' });
      throw new Error('Invalid thread.');
    }
    await updateThread(thread.id, { subforum_id: Number(id) });

    setMoveModal({ show: false, options: [] });
    pushNotification({ message: 'Thread moved.', type: 'success' });
  };

  const markUnreadNotificationsInPosts = async (
    posts: Post[],
    notificationType: string
  ): Promise<void> => {
    const unreadNotificationPostIds = posts
      .map((post) => {
        if (
          `${notificationType}:${post.id}` in notifications &&
          !notifications[`${notificationType}:${post.id}`].read
        ) {
          return post.id;
        }
        return null;
      })
      .filter((postId) => Boolean(postId));

    if (unreadNotificationPostIds.length === 0) return;

    await markNotificationsAsRead(
      unreadNotificationPostIds.map((id) => notifications[`${notificationType}:${id}`].id)
    );
    unreadNotificationPostIds.forEach((postId) => {
      dispatch(readNotification(`${notificationType}:${postId}`));
    });
  };

  const markMentionsAsRead = async () => {
    if (Object.keys(notifications).length === 0) return;

    const { posts } = thread;

    try {
      await markUnreadNotificationsInPosts(posts, 'POST_REPLY');
      await markUnreadNotificationsInPosts(posts, 'POST_MENTION');
    } catch (e) {
      console.error(e);
    }
  };

  const markThreadRead = async () => {
    const lastPostNumber = getLastPostOnPageNumber(thread.currentPage);
    await createReadThreadRequest(thread.id, lastPostNumber);
    setThread((prevThread) => ({
      ...prevThread,
      readThread: {
        lastPostNumber,
        unreadPostCount: 0,
        firstUnreadId: -1,
        isSubscription: prevThread.readThread?.isSubscription ?? false,
      },
    }));
  };

  useEffect(() => {
    setNewPostQueue([]);
    socketClient.emit('threadPosts:join', match.params.id);
    return () => {
      socketClient.emit('threadPosts:leave', match.params.id);
      dispatch(updateBackgroundRequest(null));
    };
  }, [match.params.id]);

  useEffect(() => {
    setNewPostQueue([]);
    socketClient.on('threadPost:new', (value: Post) => {
      if (value.user!.id !== userState.id) {
        if (value.page === Number(match.params.page)) {
          setNewPostQueue((prevQueue) => [...prevQueue, value]);
        }
        setThread((prevThread) => ({ ...prevThread, postCount: prevThread.postCount + 1 }));
      }
    });
    return () => {
      socketClient.off('threadPost:new');
    };
  }, [match.params.page]);

  const firstPostId = thread.posts[0]?.id;
  const lastPostId = thread.posts[thread.posts.length - 1]?.id;

  useEffect(() => {
    refreshPosts({ threadId: match.params.id, page: match.params.page, goToPost: Boolean(hash) });
  }, [match.params.id, match.params.page]);

  useEffect(() => {
    jumpToPost(postJump);
  }, [firstPostId]);

  useEffect(() => {
    if (userState?.id) {
      // handle updating Alerts (subscriptions)
      if (isSubscribed) {
        updateReadThread(true);
      } else if (postJump.goToLatest && autoSubscribe) {
        createAlert();
      } else {
        updateReadThread();
      }
    }

    // if we have a thread background, let's set it!
    dispatch(updateBackgroundRequest(thread.backgroundUrl, thread.backgroundType));
    markMentionsAsRead();
  }, [lastPostId, thread.backgroundType, thread.backgroundUrl, userState?.id]);

  const postId = Number(hash?.replace('#post-', '')) || undefined;

  const { params } = match;
  const currentPage = params.page ? Number(params.page) : 1;

  return (
    <ThreadPageComponent
      thread={thread}
      currentPage={currentPage}
      showingMoveModal={moveModal.show}
      moveModalOptions={moveModal.options}
      togglePinned={() => updateThreadStatus({ pinned: thread.pinned })}
      toggleLocked={() => updateThreadStatus({ locked: thread.locked })}
      toggleDeleted={() => updateThreadStatus({ deleted: thread.deleted })}
      showMoveModal={showMoveModal}
      deleteAlert={() => deleteAlert(thread.id)}
      createAlert={() => createAlert(true, newestSeenReadThreadPost)}
      refreshPosts={refreshPosts}
      currentUserId={userState.id}
      submitThreadMove={submitThreadMove}
      hideModal={() => setMoveModal({ show: false, options: [] })}
      params={params}
      linkedPostId={postId}
      isSubscribed={isSubscribed}
      newPostQueue={newPostQueue}
      stickyPostLink={hash && postJump.goToPost}
      addNewPosts={addNewPosts}
      markThreadRead={markThreadRead}
    />
  );
};

export default ThreadPage;
