import React from 'react';
import { Helmet } from 'react-helmet';
import styled from 'styled-components';
import { ThemeKnockoutRed, ThemeFontSizeMedium } from '../../../utils/ThemeNew';

const StyledPrivacyPolicy = styled.div`
  p {
    max-width: 1000px;
  }
  @keyframes warning-looper {
    0% {
      left: 100%;
    }
    100% {
      left: -800px;
    }
  }
  section.warning {
    height: 50px;
    background-color: ${ThemeKnockoutRed};
    white-space: nowrap;
    overflow: hidden;
    position: relative;
    h2 {
      left: 100%;
      position: absolute;
      animation-name: warning-looper;
      animation-timing-function: linear;
      animation-duration: 12s;
      animation-iteration-count: infinite;
      width: 800px;
      text-align: center;
      line-height: 50px;
      display: inline-block;
      margin: 0;
      padding: 0;
      margin-right: 20px;
      color: white;
      &::after,
      &::before {
        content: '·';
      }
    }
    h2:nth-of-type(2) {
      left: -10000px;
      animation-delay: 6s;
    }
    h2:nth-of-type(3) {
      left: -10000px;
      animation-delay: 12s;
    }
  }
  ul {
    font-size: ${ThemeFontSizeMedium};
    li {
      line-height: 1.5em;
    }
  }
  @media (max-width: 560px) {
    section.warning {
      h2 {
        animation: none;
        position: static;
        display: inline-block;
        width: 100%;
        font-size: 1.2em;
        margin: 0;
      }
    }
  }
`;

const PrivacyPolicy = () => {
  return (
    <StyledPrivacyPolicy>
      <Helmet>
        <title>Privacy Policy - Knockout!</title>
      </Helmet>
      <h1 className="underline">Privacy Policy and Terms of Use</h1>
      <p>
        If you want to use Knockout, you must read and agree to all of the below. It’s important, so
        read all of it!
      </p>
      <section>
        <h3 className="underline-small">Your Data</h3>
        <p>
          For us to function right now, we do have to (unfortunately) collect some data. In the
          future we’d prefer not to save any of this data, or to obfuscate it so that in the case
          something bad happens, it makes attackers’ lives harder.
        </p>
        <h3 className="underline-small">Data We Collect and Store</h3>
        <ul>
          <li>Username</li>
          <li>The Account ID associated with your login provider (Google, Twitter, etc)</li>
          <li>The IP(s) you were using when creating a new post or thread</li>
        </ul>
        <h3 className="underline-small">Data We Do Not Collect</h3>
        <ul>
          <li>Email Address</li>
          <li>Real name</li>
          <li>Fine location</li>
          <li>
            Age - that said, you
            <b>&nbsp;must&nbsp;</b>
            be 18 or older to use this site.
          </li>
          <li>Sex</li>
        </ul>
        <h3 className="underline-small">Data Retention</h3>
        <p>
          Hourly backups of the production databases are taken, these backups are retained for a
          month before they “rotate out” and are disposed of. Any accounts deleted will remain in
          the backups until the files expire after a month.
        </p>
        <p>
          Web server access and error logs are retained for a month before being disposed of, these
          logs contain your IP Addresses, which pages you have visited and information about your
          web browser version. These logs exist for diagnostic purposes and for investigating
          malicious traffic.
        </p>
        <p>
          IP Addresses are retained indefinitely to allow us to identify abusive users and implement
          IP bans for said users.
        </p>
        <h3 className="underline-small">Data Processing</h3>
        <p>
          Your IP Address may be crosschecked against several databases to identify traffic sources
          and rough geographical location. You may opt out of geoip processing within your account
          settings, this is an optional feature to enable public country information on your posts.
        </p>
        <h3 className="underline-small">Data Privacy</h3>
        <p>
          The limited personal information we store will never be shared with any third party
          (except law enforcement organizations upon request), and any processing of your personal
          information is handled internally with no dependencies on third parties.
        </p>
        <h3 className="underline-small">Right to Delete</h3>
        <p>We want to be as privacy-friendly as possible.</p>
        <p className="highlight">
          <b> do not post any personally-identifiable info in this site.</b>
        </p>
        <p>
          When you ask an admin for your account to be deleted, some data will be retained in our
          database in order for us to keep offering a quality service:
        </p>
        <h3 className="underline-small">DO NOT POST PERSONALLY IDENTIFIABLE INFO IN THIS SITE</h3>
        <p>
          Do not post personally identifiable info on this site. If you delete your user the posts
          you submitted to this site will be retained.
        </p>
        <h3 className="underline-small">Follow The Rules Or Get Muted</h3>
        <p>
          If you’re new to this site, know that sometimes you’ll get muted as a warning. If you want
          to minimize the chances that this will happen to you, read the rules and pay attention to
          the etiquette of other users.
        </p>
        <h3 className="underline-small">And This Can Not Be Stressed Enough</h3>
      </section>
      <section className="warning">
        <h2>DO NOT POST PERSONAL INFO</h2>
        <h2>DO NOT POST PERSONAL INFO</h2>
        <h2>DO NOT POST PERSONAL INFO</h2>
      </section>
    </StyledPrivacyPolicy>
  );
};

export default PrivacyPolicy;
