import React, { useState, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { getAlerts, deleteAlertRequest, batchDeleteAlertsRequest } from '../../services/alerts';
import AlertsListThreadItem from './components/AlertsListThreadItem';
import BlankSlate from '../../components/BlankSlate';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeFontSizeHuge,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import { isLoggedIn } from '../../components/LoggedInOnly';
import { pushNotification } from '../../utils/notification';
import updateSubscriptions from '../../utils/subscriptions';
import ThreadItemPlaceholder from '../../components/ThreadItemPlaceholder';
import Pagination from '../../components/Pagination';
import socketClient from '../../socketClient';

const StyledAlertsList = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  nav.subheader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .return-btn {
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    .back-and-title {
      display: flex;
      height: 30px;
      overflow: hidden;
      align-items: stretch;
      margin-right: ${ThemeHorizontalPadding};
      text-decoration: none;
    }
  }

  .subheader.pagination {
    justify-content: flex-end;
  }

  .alert-management {
    display: flex;
    align-items: flex-end;
    font-size: ${ThemeFontSizeSmall};
    background: ${ThemeBackgroundLighter};
    padding: ${ThemeVerticalPadding};
    margin-bottom: ${ThemeHorizontalPadding};

    .select-btn {
      padding: 0;
      cursor: pointer;
      color: ${ThemeTextColor};
      background: none;
      border: none;
    }

    .unsubscribe-btn {
      margin-left: auto;
      padding-bottom: 0;
      cursor: pointer;
      color: ${ThemeTextColor};
      background: none;
      border: none;
    }
  }
`;

const AlertsList = () => {
  const {
    params: { page = 1 },
  } = useRouteMatch();
  const [alerts, setAlerts] = useState([]);
  const [totalAlerts, setTotalAlerts] = useState(0);
  const [alertsLoaded, setAlertsLoaded] = useState(false);
  const [allAlertIds, setAllAlertIds] = useState([]);
  const [alertIdsSelected, setAlertIdsSelected] = useState([]);
  const dispatch = useDispatch();
  const nsfwFilterEnabled = useSelector((state) => state.settings.nsfwFilter);

  useEffect(() => {
    const getAlertsList = async () => {
      const results = await getAlerts(nsfwFilterEnabled, page);
      setAlerts(results.alerts);
      setTotalAlerts(results.totalAlerts);
      setAllAlertIds(results.ids);
      setAlertsLoaded(true);
    };

    getAlertsList();
  }, [page]);

  useEffect(() => {
    updateSubscriptions(dispatch, alerts);
  }, [alerts, dispatch]);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  const markUnreadAction = async (id) => {
    try {
      await deleteAlertRequest({ threadId: id });
      socketClient.emit('subscribedThreadPosts:leave', id);
      const results = await getAlerts(nsfwFilterEnabled, page);
      setAlerts(results.alerts);
      setTotalAlerts(results.totalAlerts);
      setAlertIdsSelected(alertIdsSelected.filter((alertId) => alertId !== id));
      pushNotification({ message: 'Unsubscribed from thread.' });
    } catch (err) {
      console.error(err);
      pushNotification({ message: 'Could not unsubscribe from thread.', type: 'error' });
    }
  };

  const checkboxAction = (id, checked) => {
    let newAlertIds;
    if (!checked) {
      newAlertIds = alertIdsSelected.filter((alertId) => alertId !== id);
    } else {
      newAlertIds = [...alertIdsSelected, id];
    }
    setAlertIdsSelected(newAlertIds);
  };

  const selectPage = () => {
    setAlertIdsSelected([...new Set(alertIdsSelected.concat(alerts.map((alert) => alert.id)))]);
  };

  const selectAll = () => {
    setAlertIdsSelected(allAlertIds);
  };

  const deselectAll = () => {
    setAlertIdsSelected([]);
  };

  const handleUnsubscribe = async () => {
    await batchDeleteAlertsRequest({ threadIds: alertIdsSelected });
    socketClient.emit('subscribedThreadPosts:leave', alertIdsSelected);

    const totalAlertsDeleted = alertIdsSelected.length;
    const results = await getAlerts(nsfwFilterEnabled, page);

    setAlertIdsSelected([]);
    setAlerts(results.alerts);
    setTotalAlerts(results.totalAlerts);
    setAllAlertIds(results.ids);
    const threadPlural = totalAlertsDeleted > 1 ? 'threads' : 'thread';
    pushNotification({ message: `Unsubscribed from ${totalAlertsDeleted} ${threadPlural}.` });
  };

  let alertsContent;

  if (alertsLoaded && totalAlerts === 0) {
    alertsContent = <BlankSlate resourceNamePlural="subscriptions" />;
  } else if (!alertsLoaded) {
    alertsContent = Array(16)
      .fill(1)
      // eslint-disable-next-line react/no-array-index-key
      .map((item, index) => <ThreadItemPlaceholder key={`p${index}`} />);
  } else {
    alertsContent = alerts.map((alert) => {
      return (
        <AlertsListThreadItem
          key={alert.id}
          thread={{
            ...alert.thread,
            readThread: {
              firstUnreadId: alert.firstUnreadId,
              lastPostNumber: 0,
              unreadPostCount: alert.unreadPosts,
            },
          }}
          markUnreadAction={() => markUnreadAction(alert.id)}
          checkboxAction={(e) => checkboxAction(alert.id, e.target.checked)}
          checked={alertIdsSelected.some((id) => id === alert.id)}
        />
      );
    });
  }

  const isSelectingAll = alertIdsSelected.length === allAlertIds.length;

  return (
    <StyledAlertsList>
      <Helmet>
        <title>Subscriptions - Knockout!</title>
      </Helmet>
      <h2>Subscriptions</h2>

      <nav className="subheader">
        <span className="back-and-title">
          <div className="left">
            <Link className="return-btn" to="/">
              <i className="fa-solid fa-angle-left" />
              <span>Home</span>
            </Link>
          </div>
        </span>
        <Pagination
          showNext
          pagePath="/subscriptions/"
          totalPosts={totalAlerts}
          currentPage={page}
          pageSize={20}
        />
      </nav>
      {alertIdsSelected.length > 0 && (
        <div className="alert-management">
          <div className="select-info">
            <span>{`${alertIdsSelected.length} selected`}</span>
            <span>{' • '}</span>
            <button type="button" title="Select page" className="select-btn" onClick={selectPage}>
              Select page
            </button>
            <span>{' • '}</span>
            <button type="button" title="Select page" className="select-btn" onClick={deselectAll}>
              Deselect all
            </button>
            <span>{' • '}</span>
            <button
              type="button"
              title="Select all subscriptions"
              className="select-btn"
              onClick={selectAll}
            >
              {isSelectingAll
                ? `All ${totalAlerts} subscriptions selected`
                : `Select all ${totalAlerts} subscriptions`}
            </button>
          </div>
          <button
            type="button"
            title="Unsubscribe"
            className="unsubscribe-btn"
            onClick={handleUnsubscribe}
          >
            <i className="fa-solid fa-trash" />
            <span>{` Unsubscribe`}</span>
          </button>
        </div>
      )}
      {alertsContent}
      <nav className="subheader pagination">
        <Pagination
          showNext
          pagePath="/subscriptions/"
          totalPosts={totalAlerts}
          currentPage={page}
          pageSize={20}
        />
      </nav>
    </StyledAlertsList>
  );
};

export default AlertsList;
