import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import styled, { css } from 'styled-components';
import { lighten, transparentize } from 'polished';
import { CalendarEvent } from 'knockout-schema';
import {
  ThemeBackgroundDarker,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeMainBackgroundColor,
  ThemeTextColor,
  ThemeVerticalPadding,
  ThemeHighlightWeaker,
} from '../../../utils/ThemeNew';

dayjs.extend(relativeTime);

const EVENT_TITLE_MAX_DISPLAY_LENGTH = 30;

const StyledMiniCalendarDay = styled.div<{
  hasEvents: boolean;
  isCurrentDay: boolean;
  differentMonth: boolean;
}>`
  flex: 1 1 12.5%;
  border: 1px solid ${(props) => transparentize(0.6, ThemeTextColor(props))};
  ${(props) =>
    props.hasEvents &&
    css`
      box-shadow: 0 0 1px 2px ${ThemeHighlightWeaker(props)} inset;
    `}
  padding: 2px;
  font-size: ${ThemeFontSizeSmall};
  min-height: 125px;
  min-width: 125px;
  ${(props) => {
    if (props.isCurrentDay) {
      return css`
        background-color: ${lighten(0.2, ThemeBackgroundDarker(props))};
      `;
    }
    if (props.differentMonth) {
      return css`
        background-color: ${transparentize(0.1, ThemeBackgroundDarker(props))};
      `;
    }
    return 'none;';
  }}

  &:hover {
    .day-header {
      .create-event-button {
        display: inline-block;
      }
    }

    background-color: ${(props) =>
      !props.isCurrentDay ? lighten(0.3, ThemeBackgroundDarker(props)) : 'none'};
  }

  .day-header {
    display: flex;
    justify-content: space-between;

    .day-title {
      background: none;
      border: none;
      ${(props) => (props.hasEvents ? 'cursor: pointer' : 'pointer-events: none')};
      color: ${(props) => transparentize(0.3, ThemeTextColor(props))};
      margin: calc(${ThemeVerticalPadding} / 2) calc(${ThemeHorizontalPadding} / 2);
      padding: 0;

      &:hover {
        color: ${ThemeTextColor};
      }
    }

    .create-event-button {
      display: none;
      background: none;
      border: none;
      cursor: pointer;
      color: ${ThemeTextColor};

      &:hover {
        opacity: 0.5;
      }
    }
  }

  .events-list {
    display: flex;
    flex-direction: column;
    margin-top: ${ThemeVerticalPadding};

    .event {
      font-size: calc(${ThemeFontSizeSmall} * 0.9);
      cursor: pointer;
      margin-left: calc(${ThemeVerticalPadding} / 2);
      margin-bottom: calc(${ThemeVerticalPadding} / 2);
      margin-right: 5px;
      padding: calc(${ThemeVerticalPadding} / 2) calc(${ThemeHorizontalPadding} / 2);
      background: ${ThemeMainBackgroundColor};
    }
  }
`;

interface MiniCalendarDayProps {
  date: Date;
  dayEvents: CalendarEvent[];
  onEventClick: (event: CalendarEvent) => void;
  onDayClick: () => void;
  differentMonth: boolean;
  canCreate: boolean;
  onCreateClick: () => void;
}

const MiniCalendarDay = ({
  date,
  dayEvents,
  onEventClick,
  onDayClick,
  differentMonth,
  canCreate,
  onCreateClick,
}: MiniCalendarDayProps) => {
  const isCurrentDay = date.toDateString() === new Date().toDateString();
  const isInPast = date.getTime() < new Date().getTime();
  const dayName = date.toLocaleString('en-us', { weekday: 'long' });
  const dayNumber = date.getDate();

  return (
    <StyledMiniCalendarDay
      isCurrentDay={isCurrentDay}
      hasEvents={dayEvents.length > 0}
      differentMonth={differentMonth}
    >
      <div className="day-header">
        <button type="button" onClick={onDayClick} className="day-title">
          {`${dayName} ${dayNumber}`}
        </button>

        {canCreate && !isInPast && (
          <button
            type="button"
            onClick={onCreateClick}
            className="create-event-button"
            title={`Create event for ${dayName} ${dayNumber}`}
          >
            <span className="fa-solid fa-plus" />
          </button>
        )}
      </div>

      <div className="events-list">
        {dayEvents.slice(0, 5).map((event) => {
          let truncatedTitle = event.title.substring(0, EVENT_TITLE_MAX_DISPLAY_LENGTH);
          if (event.title.length > EVENT_TITLE_MAX_DISPLAY_LENGTH) {
            truncatedTitle = `${truncatedTitle}...`;
          }
          return (
            <div
              key={event.title}
              tabIndex={0}
              role="button"
              className="event"
              onKeyUp={(e) => {
                if (e.key === 'Enter') onEventClick(event);
              }}
              onClick={() => onEventClick(event)}
            >
              {truncatedTitle}
            </div>
          );
        })}
      </div>
    </StyledMiniCalendarDay>
  );
};

export default MiniCalendarDay;
