/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { connect, useSelector } from 'react-redux';
import { Redirect, Route, Switch, useLocation } from 'react-router-dom';
import { Slide, ToastContainer } from 'react-toastify';
import Snowfall from 'react-snowfall';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import routes from '../../routes';
import ScrollOnRouteChange from './components/ScrollOnRouteChange';
import { GlobalStyle, Background, FlexWrapper } from './style';
import PassiveAggressiveness from './components/PassiveAggressiveness';
import { loadUserFromStorage } from '../../services/user';
import { DELETED_USER_USERNAME } from '../../utils/deletedUser';
import { isHolidays } from '../../utils/eventDates';

export const ConnectedGlobalStyle = connect(({ background }) => ({
  backgroundUrl: background.url,
}))(GlobalStyle);

export const ConnectedBackground = connect(({ background }) => ({
  url: background.url,
  bgType: background.type,
}))(Background);

const MainView = () => {
  const hasMotd = useSelector((state) => state.style.motd);

  const location = useLocation();
  const isUserSetup = location.pathname === '/usersetup';
  const loggingOut = location.pathname === '/logout';
  const user = loadUserFromStorage();
  const redirectToUserSetup = !isUserSetup && !loggingOut && user && !user.username;
  const holidayTheme = useSelector((state) => state.style.holidayTheme);
  const punchyLabsEnabled = useSelector((state) => state.settings.punchyLabs);
  const enableSnow = isHolidays() && holidayTheme;

  const userIsDeleted = user && user.username === DELETED_USER_USERNAME;

  if (userIsDeleted) {
    return <Redirect to="/logout" />;
  }

  if (redirectToUserSetup) {
    return <Redirect to="/usersetup" />;
  }

  const getPunchyLabsNoticeNotRead = () => {
    return punchyLabsEnabled && localStorage.getItem('punchyLabsNoticeRead') !== 'true';
  };

  return (
    <FlexWrapper hasMotd={hasMotd}>
      <ConnectedGlobalStyle />

      <Header />

      {enableSnow && (
        <Snowfall
          snowflakeCount={75}
          speed={[0.5, 1]}
          wind={[0.5, 1]}
          radius={[0.5, 3]}
          rotationSpeed={[0, 0.5]}
          changeFrequency={5000}
          style={{
            position: 'fixed',
            width: '100vw',
            height: '100vh',
            opacity: '0.6',
            zIndex: '999',
          }}
        />
      )}

      <ScrollOnRouteChange>
        <Switch>
          {routes.map((route) => (
            <Route key={route.name} {...route} />
          ))}
          <Redirect from="*" to="/" />
        </Switch>
      </ScrollOnRouteChange>

      <Footer />

      <ToastContainer transition={Slide} />

      <ConnectedBackground />

      {getPunchyLabsNoticeNotRead() && <PassiveAggressiveness />}
    </FlexWrapper>
  );
};
export default MainView;
