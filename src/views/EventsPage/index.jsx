import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet';
import { EventType, THREAD_EVENT_TYPES } from 'knockout-schema';
import getEventsList from '../../services/events';

import EventsComponent from './components/EventsComponent';
import { isLoggedIn } from '../../components/LoggedInOnly';
import socketClient from '../../socketClient';
import { getHiddenThreads } from '../../services/hiddenThreads';

const EventsPage = () => {
  const [events, setEvents] = useState([]);
  const [eventsLoaded, setEventsLoaded] = useState(false);
  const [initialLoad, setInitialLoad] = useState(true);
  const [hiddenThreadIds, setHiddenThreadIds] = useState([]);

  const hideEvent = (event) => {
    if (THREAD_EVENT_TYPES.includes(event.type)) {
      return hiddenThreadIds.includes(event.data?.id);
    }
    if (
      [EventType.POST_CREATED, EventType.RATING_CREATED, EventType.USER_BANNED].includes(event.type)
    ) {
      return hiddenThreadIds.includes(event.data?.thread?.id);
    }
    return false;
  };

  const getEvents = async () => {
    setEvents(await getEventsList());
    setEventsLoaded(true);
  };

  const getHiddenThreadIds = async () => {
    const threads = await getHiddenThreads();
    setHiddenThreadIds(threads.map((thread) => thread.id));
  };

  useEffect(() => {
    getHiddenThreadIds();
    getEvents();
    socketClient.emit('events:join');
    socketClient.on('events:new', (event) => {
      setInitialLoad(false);
      if (!hideEvent(event)) {
        setEvents((prevEvents) => [event, ...prevEvents].slice(0, 300));
      }
    });

    return () => {
      socketClient.emit('events:leave');
      socketClient.off('events:new');
    };
  }, []);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  return (
    <>
      <Helmet>
        <title>Ticker - Knockout!</title>
      </Helmet>
      <EventsComponent events={events} eventsLoaded={eventsLoaded} initialLoad={initialLoad} />
    </>
  );
};

export default EventsPage;
