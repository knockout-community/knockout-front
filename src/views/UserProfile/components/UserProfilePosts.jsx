import React from 'react';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';
import Post from '../../../components/Post';
import PostProfilePlaceholder from '../../../components/PostProfilePlaceholder';
import { getUserPosts } from '../../../services/user';
import useFetchedData from './useFetchedData';
import UserContentContainer from './UserContentContainer';
import { isLoggedIn } from '../../../components/LoggedInOnly';

const UserProfilePosts = ({ posts, showRatings, user, match }) => {
  const [currentPosts, loading, fetchData] = useFetchedData(
    posts,
    posts.posts,
    match,
    getUserPosts
  );

  const { ratingsXray } = useSelector((state) => state.settings);

  const ratingsXrayEnabled = isLoggedIn() && ratingsXray;

  let postContent = Array(16)
    .fill(1)
    // eslint-disable-next-line react/no-array-index-key
    .map((item, index) => <PostProfilePlaceholder key={`p${index}`} />);
  if (!loading)
    postContent = currentPosts.posts.map((post) => (
      <Post
        key={post.id}
        hideUserWrapper
        hideControls
        byCurrentUser
        ratings={showRatings ? post.ratings : []}
        threadId={post.thread.id || post.thread}
        threadInfo={post.thread.id && post.thread}
        mentionUsers={post.mentionUsers}
        postId={post.id}
        postBody={post.content}
        postDate={post.createdAt}
        postEdited={post.updatedAt}
        postPage={post.page}
        threadPage={1}
        user={user}
        profileView
        ratingsXray={ratingsXrayEnabled}
      />
    ));

  return (
    <UserContentContainer
      fetchData={fetchData}
      total={currentPosts.totalPosts}
      currentPage={currentPosts.currentPage}
      content={postContent}
    />
  );
};

UserProfilePosts.propTypes = {
  posts: PropTypes.shape({
    posts: PropTypes.arrayOf(
      PropTypes.shape({
        content: PropTypes.string.isRequired,
        createdAt: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
        page: PropTypes.number,
        thread: PropTypes.shape({
          id: PropTypes.number.isRequired,
          title: PropTypes.string.isRequired,
        }),
      })
    ),
    totalPosts: PropTypes.number,
    currentPage: PropTypes.number.isRequired,
  }).isRequired,
  user: PropTypes.shape({}).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  showRatings: PropTypes.bool.isRequired,
};

export default UserProfilePosts;
