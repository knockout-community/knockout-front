import React from 'react';
import { Thread } from 'knockout-schema';
import { match as Match } from 'react-router-dom';
import ThreadItem from '../../../components/ThreadItem';
import ThreadItemPlaceholder from '../../../components/ThreadItemPlaceholder';
import { getUserThreads } from '../../../services/user';
import useFetchedData from './useFetchedData';
import UserContentContainer from './UserContentContainer';

interface UserProfileThreadsProps {
  threads: { threads: Thread[] };
  match: Match;
}

const UserProfileThreads = ({ threads, match }: UserProfileThreadsProps) => {
  const [currentThreads, loading, fetchData] = useFetchedData(
    threads,
    threads.threads,
    match,
    getUserThreads
  );

  let threadContent = Array(16)
    .fill(1)
    // eslint-disable-next-line react/no-array-index-key
    .map((item, index) => <ThreadItemPlaceholder minimal key={`p${index}`} />);
  if (!loading)
    threadContent = currentThreads.threads.map((thread) => (
      <ThreadItem key={thread.id} thread={thread} minimal />
    ));
  return (
    <UserContentContainer
      fetchData={fetchData}
      total={currentThreads.totalThreads}
      currentPage={currentThreads.currentPage}
      content={threadContent}
    />
  );
};

export default UserProfileThreads;
