import React from 'react';
import { ThreadWithLastPost } from 'knockout-schema';
import ThreadItemPlaceholder from '../../../components/ThreadItemPlaceholder';
import BlankSlate from '../../../components/BlankSlate';
import ThreadItem from '../../../components/ThreadItem';

interface ThreadSearchResultsProps {
  totalThreads?: number;
  threads?: ThreadWithLastPost[];
  loading?: boolean;
}

const ThreadSearchResults = ({
  totalThreads = 0,
  threads = [],
  loading = false,
}: ThreadSearchResultsProps) => {
  let resultsContent;

  if (totalThreads === 0 && !loading) {
    resultsContent = <BlankSlate resourceNamePlural="threads" />;
  } else if (loading) {
    resultsContent = Array(10)
      .fill(1)
      // eslint-disable-next-line react/no-array-index-key
      .map((_, index) => <ThreadItemPlaceholder key={`p${index}`} />);
  } else {
    resultsContent = threads?.map((thread) => {
      return <ThreadItem key={thread.id} thread={thread} />;
    });
  }

  return resultsContent;
};

export default React.memo(ThreadSearchResults);
