import React, { useState, useEffect } from 'react';
import { Link, useRouteMatch, useHistory } from 'react-router-dom';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import { threadSearch } from '../../services/threadSearch';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeFontSizeMedium,
  ThemeFontSizeHuge,
  ThemeMainBackgroundColor,
  ThemeHighlightWeaker,
  ThemeKnockoutRed,
} from '../../utils/ThemeNew';
import { MobileMediaQuery } from '../../components/SharedStyles';
import { getTags } from '../../services/tags';
import { getSubforumList } from '../../services/subforums';
import { Button } from '../../components/Buttons';
import Pagination from '../../components/Pagination';
import ThreadSearchResults from './components/ThreadSearchResults';
import { pushSmartNotification } from '../../utils/notification';

const StyledThreadSearchPage = styled.section`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h1 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  nav.subheader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .return-btn {
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    .back-and-title {
      display: flex;
      height: 30px;
      overflow: hidden;
      align-items: stretch;
      margin-right: ${ThemeHorizontalPadding};
      text-decoration: none;
    }
  }

  .subheader.pagination {
    justify-content: flex-end;
  }

  .panel {
    background-color: ${ThemeBackgroundDarker};
    padding-bottom: ${ThemeVerticalPadding};

    ${MobileMediaQuery} {
      width: auto;
      flex-basis: 2;
    }

    .header {
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      padding: calc(${ThemeVerticalPadding} * 2) ${ThemeHorizontalPadding};

      .form-input {
        margin: 0 ${ThemeHorizontalPadding};
        padding-right: 0;
      }

      .thread-title-form-error {
        color: ${ThemeKnockoutRed};
        font-size: ${ThemeFontSizeSmall};
        margin-top: 0;
        margin-bottom: 0;
        height: 0;
      }

      label {
        margin-right: ${ThemeHorizontalPadding};
        margin-bottom: ${ThemeHorizontalPadding};
      }

      .search-field {
        background: ${ThemeBackgroundLighter};
        border: none;
        outline: none;
        width: 100%;
        line-height: 22px;
        font-size: ${ThemeFontSizeMedium};
        color: ${ThemeTextColor};
        padding: calc(${ThemeVerticalPadding} / 2) 0;
        padding-left: ${ThemeHorizontalPadding};
        margin-top: ${ThemeVerticalPadding};
        margin-bottom: 20px;
      }

      .search-select {
        border: none;
        width: 100%;
        padding: 5px;
        line-height: 1;
        outline: none;
        font-size: ${ThemeFontSizeMedium};
        color: inherit;
        background: ${ThemeBackgroundLighter};
        height: 30px;
        margin-bottom: 20px;
      }

      .search-select:disabled {
        opacity: 0.5;
      }

      .search-button {
        background: ${ThemeHighlightWeaker};
        border: none;
        outline: none;
        cursor: pointer;
        width: 100%;
        line-height: 1;
        height: 30px;
        color: ${ThemeTextColor};
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        margin-top: calc(${ThemeVerticalPadding} / 2);

        &:disabled {
          pointer-events: none;
          opacity: 0.5;
        }
      }

      ${MobileMediaQuery} {
        flex-direction: column;
        align-content: flex-start;
        align-items: flex-start;
      }
    }

    .searchHeader {
      margin: ${ThemeVerticalPadding} calc(${ThemeHorizontalPadding} * 2);
    }

    .body {
      background-color: ${ThemeMainBackgroundColor};

      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    }
  }
`;

const MIN_TITLE_LENGTH = 5;

const SORT_BY_OPTIONS = [
  {
    name: 'Relevance',
    value: 'relevance',
  },
  {
    name: 'Thread Updated',
    value: 'updated_at',
  },
  {
    name: 'Thread Created',
    value: 'created_at',
  },
];

const SORT_ORDER_OPTIONS = [
  {
    name: 'Descending',
    value: 'desc',
  },
  {
    name: 'Ascending',
    value: 'asc',
  },
];

const ThreadSearchPage = () => {
  const {
    params: { page = 1, title: paramTitle },
  } = useRouteMatch();

  const history = useHistory();

  const [title, setTitle] = useState(paramTitle || '');
  const [subforums, setSubforums] = useState([]);
  const [subforumId, setSubforumId] = useState(null);
  const [tags, setTags] = useState([]);
  const [tagId, setTagId] = useState(null);
  const [showLockedThreads, setShowLockedThreads] = useState(false);
  const [sortBy, setSortBy] = useState(SORT_BY_OPTIONS[0].value);
  const [sortOrder, setSortOrder] = useState(SORT_ORDER_OPTIONS[0].value);
  const [loading, setLoading] = useState(false);
  const [threads, setThreads] = useState(null);
  const [totalThreads, setTotalThreads] = useState(0);
  const [searched, setSearched] = useState(false);

  const nsfwFilterEnabled = useSelector((state) => state.settings.nsfwFilter);

  const searchValid = title?.length >= MIN_TITLE_LENGTH;

  const search = async () => {
    if (!searchValid) {
      return;
    }

    setLoading(true);
    setThreads([]);

    const parsedSubforumId = Number(subforumId) || null;
    const parsedTagId = Number(tagId) || null;
    const parsedPage = Number(page) || 1;

    try {
      const result = await threadSearch({
        title: paramTitle,
        subforumId: parsedSubforumId,
        tagIds: parsedTagId ? [parsedTagId] : null,
        sortBy,
        sortOrder,
        page: parsedPage,
        locked: showLockedThreads,
      });
      setThreads(result.threads);
      setTotalThreads(result.totalThreads);
    } catch (e) {
      pushSmartNotification({ error: 'Could not search. Try again in a few moments.' });
    }

    setSearched(true);
    setLoading(false);
  };

  const handleEnterKeyShortcut = (e) => {
    if (e.key === 'Enter' && searchValid && title !== paramTitle) {
      history.push(`/threadsearch/${title}/1`);
    }
  };

  useEffect(() => {
    const getSubforums = async () => {
      const subforumList = await getSubforumList(nsfwFilterEnabled);
      setSubforums(subforumList);
    };

    const getTagList = async () => {
      const tagList = await getTags();
      setTags(tagList);
    };
    getSubforums();
    getTagList();
  }, []);

  useEffect(() => {
    search();
  }, [page, paramTitle]);

  useEffect(() => {
    window.addEventListener('keydown', handleEnterKeyShortcut);
    return () => window.removeEventListener('keydown', handleEnterKeyShortcut);
  }, [page, title]);

  useEffect(() => {
    if (sortBy === 'relevance') {
      setSortOrder('desc');
    }
  }, [sortBy]);

  return (
    <StyledThreadSearchPage>
      <Helmet>
        <title>Search for a Thread</title>
      </Helmet>

      <h1>Search for a Thread</h1>

      <nav className="subheader">
        <span className="back-and-title">
          <div className="left">
            <Link className="return-btn" to="/">
              <i className="fa-solid fa-angle-left" />
              <span>Home</span>
            </Link>
          </div>
        </span>
      </nav>

      <div className="panel">
        <div className="header">
          <div className="form-input">
            <label aria-label="title" htmlFor="title">
              Thread Title
              <input
                id="title"
                className="search-field"
                required
                title={`Thread title - ${MIN_TITLE_LENGTH} characters minimum`}
                type="text"
                value={title}
                onChange={(e) => setTitle(e.target.value)}
                placeholder={`At least ${MIN_TITLE_LENGTH} characters`}
              />
            </label>
            {searched && !searchValid && (
              <p className="thread-title-form-error">
                {`Thread title must be at least ${MIN_TITLE_LENGTH} characters`}
              </p>
            )}
          </div>
          <div className="form-input">
            <label aria-label="subforum" htmlFor="subforum">
              Subforum
              <select
                id="subforum"
                className="search-field search-select"
                value={subforumId}
                onChange={(e) => setSubforumId(e.target.value)}
                placeholder="Subforum"
              >
                <option value="">Any subforum</option>
                {subforums.map((subforum) => (
                  <option key={`subforum_${subforum.id}`} value={subforum.id}>
                    {subforum.name}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="form-input">
            <label aria-label="tag" htmlFor="tag">
              Tag
              <select
                id="tag"
                className="search-field search-select"
                value={tagId}
                onChange={(e) => setTagId(e.target.value)}
                placeholder="Tag"
              >
                <option value="">Any tag</option>
                {tags.map((tag) => (
                  <option key={`tag_${tag.id}`} value={tag.id}>
                    {tag.name}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="form-input">
            <label aria-label="show-locked-threads" htmlFor="show-locked-threads">
              Show Locked Threads
              <select
                id="show-locked-threads"
                className="search-field search-select"
                value={showLockedThreads ? 'Yes' : 'No'}
                onChange={(e) => setShowLockedThreads(e.target.value === 'Yes')}
                placeholder="No"
              >
                {['Yes', 'No'].map((option) => (
                  <option key={`showLockedThreads_${option}`} value={option}>
                    {option}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="form-input">
            <label aria-label="sort-by" htmlFor="sort-by">
              Sort By
              <select
                id="sort-by"
                className="search-field search-select"
                value={sortBy}
                onChange={(e) => setSortBy(e.target.value)}
                placeholder="Sort By"
              >
                {SORT_BY_OPTIONS.map((sortByOption) => (
                  <option key={`sortBy_${sortByOption.value}`} value={sortByOption.value}>
                    {sortByOption.name}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="form-input">
            <label aria-label="sort-order" htmlFor="sort-order">
              Sort Order
              <select
                id="sort-order"
                className="search-field search-select"
                value={sortOrder}
                onChange={(e) => setSortOrder(e.target.value)}
                disabled={sortBy === 'relevance'}
                placeholder="Sort Order"
              >
                {SORT_ORDER_OPTIONS.map((sortOrderOption) => (
                  <option key={`sortOrder_${sortOrderOption.value}`} value={sortOrderOption.value}>
                    {sortOrderOption.name}
                  </option>
                ))}
              </select>
            </label>
          </div>
          <div className="form-input">
            <Button
              className="search-button"
              type="submit"
              onClick={() => {
                if (title !== paramTitle) {
                  history.push(`/threadsearch/${title}/1`);
                } else {
                  search();
                }
              }}
              disabled={!searchValid || loading}
            >
              {`${loading ? 'Searching...' : 'Search'}`}
            </Button>
          </div>
        </div>
        {searched && (
          <>
            <div className="searchHeader">
              <h2>{`Search Results (${totalThreads})`}</h2>
            </div>

            <div className="body">
              <nav className="subheader pagination">
                <Pagination
                  showNext
                  pagePath={`/threadsearch/${title}/`}
                  totalPosts={totalThreads}
                  currentPage={parseInt(page, 10) || 1}
                  pageSize={10}
                />
              </nav>
              <ThreadSearchResults
                totalThreads={totalThreads}
                threads={threads}
                loading={loading}
              />
              <nav className="subheader pagination">
                <Pagination
                  showNext
                  pagePath={`/threadsearch/${title}/`}
                  totalPosts={totalThreads}
                  currentPage={parseInt(page, 10) || 1}
                  pageSize={10}
                />
              </nav>
            </div>
          </>
        )}
      </div>
    </StyledThreadSearchPage>
  );
};

export default ThreadSearchPage;
