import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import ModerationReportDetail from './ModerationReportDetail';
import ModerationReportSummary from './ModerationReportSummary';

const ModerationReports = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route exact path={path}>
        <ModerationReportSummary />
      </Route>
      <Route path={`${path}/:id`}>
        <ModerationReportDetail />
      </Route>
    </Switch>
  );
};

export default ModerationReports;
