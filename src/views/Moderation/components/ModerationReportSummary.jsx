import React, { useEffect, useState } from 'react';
import { getReports } from '../../../services/reports';
import ReportSummary from './ReportSummary';

const ModerationReportSummary = () => {
  const [reports, setReports] = useState([]);

  useEffect(() => {
    async function getAllReports() {
      setReports(await getReports());
    }
    getAllReports();
  }, []);

  return (
    <div>
      {reports.map((report) => (
        <ReportSummary key={report.id} report={report} />
      ))}
    </div>
  );
};

export default ModerationReportSummary;
