import React, { useState } from 'react';
import {
  Panel,
  PanelHeader,
  PanelSearchField,
  PanelSearchButton,
  PanelBody,
  PanelBodyPlaceholder,
} from './style';
import { getUsernamesByIp } from '../../../services/moderation';

const IpLookupPanel = () => {
  const [ip, setIp] = useState('');
  const [results, setResults] = useState([]);
  const [searched, setSearched] = useState(false);

  const search = async () => {
    const result = await getUsernamesByIp(ip);
    setResults(result);
    setSearched(true);
  };

  return (
    <Panel>
      <PanelHeader>
        <PanelSearchField
          type="text"
          value={ip}
          onChange={(e) => setIp(e.target.value)}
          onKeyDown={(e) => e.key === 'Enter' && search()}
          placeholder="Search user by IP"
        />
        <PanelSearchButton onClick={search}>Search</PanelSearchButton>
      </PanelHeader>
      {searched && (
        <PanelBody>
          <table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Username</th>
                <th>Address</th>
              </tr>
            </thead>
            <tbody>
              {results.map((item) => (
                <tr key={item.id}>
                  <td>{item.id}</td>
                  <td>{item.username}</td>
                  <td>
                    <span>{item.ip_address}</span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </PanelBody>
      )}
      {!searched && (
        <PanelBody>
          <PanelBodyPlaceholder>Please enter a search term above</PanelBodyPlaceholder>
        </PanelBody>
      )}
    </Panel>
  );
};

export default IpLookupPanel;
