import React from 'react';
import styled from 'styled-components';
import { Panel, PanelTitle } from '../../../components/Panel';
import paymentPortal from '../../../services/paymentPortal';
import { scrollToTop } from '../../../utils/pageScroll';
import { ThemeHighlightStronger } from '../../../utils/ThemeNew';

const StyledManagementButton = styled.button`
  display: block;
  position: relative;
  background: ${ThemeHighlightStronger};
  color: white;
  border: none;
  border-radius: 5px;
  margin: 9px auto 0 auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

interface HiddenThreadsContainerProps {
  showChangeUsernameModal: Function;
  showHiddenThreadsModal: Function;
  showPaymentPortalButton: boolean;
  paymentPortalReturnUrl: string;
}

const AccountManagementContainer = ({
  showChangeUsernameModal,
  showHiddenThreadsModal,
  showPaymentPortalButton,
  paymentPortalReturnUrl,
}: HiddenThreadsContainerProps) => {
  const handleManagePaymentsClick = async () => {
    const paymentPortalUrl = (await paymentPortal(paymentPortalReturnUrl)).url;
    window.location.replace(paymentPortalUrl);
  };

  return (
    <Panel>
      <PanelTitle title="Account Management">Account Management</PanelTitle>
      <StyledManagementButton
        onClick={() => {
          showChangeUsernameModal();
          scrollToTop();
        }}
        type="button"
      >
        <i className="fa-solid fa-user-edit" />
        Change Username
      </StyledManagementButton>
      <StyledManagementButton onClick={() => showHiddenThreadsModal()} type="button">
        <i className="fa-solid fa-eye-slash" />
        Manage Hidden Threads
      </StyledManagementButton>
      {showPaymentPortalButton && (
        <StyledManagementButton onClick={() => handleManagePaymentsClick()} type="button">
          <i className="fa-solid fa-dollar-sign" />
          Manage Payments
        </StyledManagementButton>
      )}
    </Panel>
  );
};

export default AccountManagementContainer;
