import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { User } from 'knockout-schema';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeHuge,
} from '../../utils/ThemeNew';
import { getCommunityTeam } from '../../services/communityTeam';
import MiniProfile from '../../components/MiniProfile';

const StyledCommunityTeamPage = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  .users {
    display: flex;
    flex-wrap: wrap;
  }
`;

const CommunityTeamPage = () => {
  const [users, setUsers] = useState<Array<User>>([]);

  useEffect(() => {
    const fetchUsers = async () => {
      const result = await getCommunityTeam();
      setUsers(result);
    };
    fetchUsers();
  }, []);

  return (
    <StyledCommunityTeamPage>
      <Helmet>
        <title>Community Team - Knockout!</title>
      </Helmet>
      <h2>Community Team</h2>

      <div className="users">
        {users.map((user) => (
          <MiniProfile key={user.id} user={user as any} showOnHover={false} />
        ))}
      </div>
    </StyledCommunityTeamPage>
  );
};

export default CommunityTeamPage;
