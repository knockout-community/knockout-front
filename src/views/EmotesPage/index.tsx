import React from 'react';
import styled from 'styled-components';
import {
  ThemeTextColor,
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
} from '../../utils/ThemeNew';
import Emote, { EmoteDefinition } from '../../components/KnockoutBB/components/Emote';
import EmoteListJson from '../../utils/emotesList.json';
import { pushSmartNotification } from '../../utils/notification';

const PageWrapper = styled.section`
  padding: 0 15px;

  .emote-grid {
    display: grid;
    gap: 8px;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
    justify-items: stretch;

    .cell {
      display: flex;
      align-items: center;
      justify-content: space-between;
      flex-direction: column;
      background: ${ThemeBackgroundLighter} none repeat scroll 0% 0%;
      padding: 8px;
      gap: 8px;
      border: 1px solid ${ThemeBackgroundDarker};
      cursor: pointer;

      .image {
        flex-grow: 1;
        display: flex;
        align-items: center;
        vertical-align: middle;
      }

      .label {
        color: ${ThemeTextColor};
        font-family: monospace;
      }
    }
  }
`;

const copyToClipboard = (text) => {
  navigator.clipboard.writeText(text);
  pushSmartNotification({ message: 'Emote copied to clipboard.' });
};

const EmotesPage = () => {
  const emotes = Object.entries(EmoteListJson as any as { [key: string]: EmoteDefinition })
    .filter(([key, value]) => !key.startsWith('!') && !value.hidden)
    .sort((a, b) => a[0].localeCompare(b[0]))
    .map(([key]) => (
      <button type="button" className="cell" key={key} onClick={() => copyToClipboard(`:${key}:`)}>
        <div className="image">
          <Emote name={key} />
        </div>
        <span className="label">{`:${key}:`}</span>
      </button>
    ));

  return (
    <PageWrapper>
      <h2>Emotes</h2>
      <p>List of available emotes. Click to copy the code.</p>

      <div className="emote-grid">{emotes}</div>
    </PageWrapper>
  );
};

export default EmotesPage;
