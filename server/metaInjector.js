const axios = require('axios');
const icons = require('../config/icons');

const { apiHost, appUrl } = require('../appConfig');
const { GOLD_USER, PAID_GOLD_USER } = require('../src/utils/roleCodes');

const userAgents = [
  'Googlebot',
  'Twitterbot',
  'Discordbot',
  'Telegrambot',
  'Facebook',
  'Valve',
  'Steam',
  'WhatsApp',
  'Slackbot',
  'redditbot',
  'DuckDuckBot',
  'KnockoutBot',
  'Mastodon',
  'https://github.com/sindresorhus/got',
];

const userAgentCheck = (req) =>
  userAgents.some((substring) =>
    req.headers['user-agent'].toLowerCase().includes(substring.toLowerCase())
  );

const mastodonLinkFromHandle = (handle) => {
  // if the handle doesnt include only one '@', return null
  if (handle.split('@').length !== 2) return null;

  const [user, host] = handle.split('@');
  return `https://${host}/@${user}`;
};

const threadUrlCheck = (req) =>
  req.originalUrl && /\/thread\/([0-9]+)\/?([0-9]*)/gm.exec(req.originalUrl);

const userProfileUrlCheck = (req) => req.originalUrl && /\/user\/([0-9]+)/gm.exec(req.originalUrl);

const getThreadInformation = async (req) => {
  const threadUrlTest = threadUrlCheck(req);

  if (!threadUrlTest || !threadUrlTest[1]) {
    return null;
  }

  try {
    const threadUrl = `${apiHost}/v2/threads/${threadUrlTest[1]}/metadata`;
    const thread = await axios.get(threadUrl);
    return thread.data;
  } catch {
    return null;
  }
};

const getUserInformation = async (req) => {
  const userUrlTest = userProfileUrlCheck(req);

  if (!userUrlTest || !userUrlTest[1]) {
    return null;
  }

  const userUrl = `${apiHost}/user/${userUrlTest[1]}`;

  try {
    const user = await axios.get(userUrl);
    return user.data;
  } catch {
    return null;
  }
};

const metaTagBuilder = async (req) => {
  if (!req.headers['user-agent'] || !userAgentCheck(req)) {
    return null;
  }

  const threadInfo = await getThreadInformation(req);
  const userInfo = await getUserInformation(req);

  const hasUserInfo = userInfo && userInfo.username && userInfo.id !== 8;

  if (threadInfo?.title) {
    const threadIcon = icons.find((el) => el.id === threadInfo.iconId).url;
    const threadTitle = `${threadInfo.title} - Knockout Forums`;
    const threadSubforum = threadInfo.subforumName;
    const threadDate = threadInfo.updatedAt;
    const threadCreationDate = threadInfo.createdAt;
    const threadUser = threadInfo.username;

    return {
      title: threadTitle.replace(/"/g, '&quot;'),
      description: `A thread by ${threadUser} in ${threadSubforum}`,
      type: 'article',
      url: `${appUrl}${req.originalUrl}`,
      image: `${appUrl}/${threadIcon}`,
      author: threadUser,
      date: threadDate,
      schema: {
        '@context': 'https://schema.org',
        '@type': 'Article',
        mainEntityOfPage: {
          '@type': 'WebPage',
          '@id': `${appUrl}${req.originalUrl}`,
        },
        headline: threadTitle,
        image: `${appUrl}/${threadIcon}`,
        datePublished: threadCreationDate,
        dateModified: threadDate,
        author: {
          '@type': 'Person',
          name: threadUser,
        },
        publisher: {
          '@type': 'Organization',
          name: 'Knockout Forums',
          logo: {
            '@type': 'ImageObject',
            url: 'https://i.imgur.com/oafvbr2.png',
          },
        },
      },
    };
  }

  if (hasUserInfo) {
    const userAvatar = `https://cdn.knockout.chat/image/${userInfo.avatarUrl}`;
    const userName = userInfo.username;
    const userPosts = userInfo.posts;
    const userThreads = userInfo.threads;
    const userCreationDate = new Date(userInfo.createdAt);
    const userBanned = userInfo.isBanned;
    const userBanMessage = userBanned
      ? ' This user is currently muted for doing something dumb. Shoulda read the rules.'
      : '';
    const userGoldMessage = [GOLD_USER, PAID_GOLD_USER].includes(userInfo.role?.code)
      ? ' This user is also a gold member! '
      : '';

    const joinYear = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(userCreationDate);
    const joinMonth = new Intl.DateTimeFormat('en', { month: 'short' }).format(userCreationDate);

    const profileUrl = `${apiHost}/v2/users/${userInfo.id}/profile`;
    const profile = (await axios.get(profileUrl)).data;
    const fediverseInfo = profile?.social?.fediverse;
    const fediverseUserLink = fediverseInfo ? mastodonLinkFromHandle(fediverseInfo) : null;

    return {
      title: userName,
      description: `${userName}'s profile on Knockout. They have made ${userPosts} posts and ${userThreads} threads. Huh. They've also been around since ${joinMonth} ${joinYear}.${userGoldMessage}${userBanMessage}`,
      type: 'article',
      url: `${appUrl}${req.originalUrl}`,
      image: userAvatar,
      twitterCard: true,
      author: userName,
      fediverseUserLink,
      date: new Date().toISOString(),
      schema: {
        '@context': 'https://schema.org',
        '@type': 'Article',
        mainEntityOfPage: {
          '@type': 'WebPage',
          '@id': `${appUrl}${req.originalUrl}`,
        },
        headline: userName,
        image: userAvatar,
        datePublished: userCreationDate,
        dateModified: new Date().toISOString(),
        author: {
          '@type': 'Person',
          name: userName,
        },
        publisher: {
          '@type': 'Organization',
          name: 'Knockout Forums',
          logo: {
            '@type': 'ImageObject',
            url: 'https://i.imgur.com/oafvbr2.png',
          },
        },
      },
    };
  }

  return null;
};

module.exports = metaTagBuilder;
