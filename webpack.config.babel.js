const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const { ESBuildMinifyPlugin } = require('esbuild-loader')
const appConfig = require('./appConfig');

/**
 * @type {import('webpack').Configuration}
 */
module.exports = {
  resolve: {
    extensions: ['.jsx', '.js', '.tsx', '.ts'],
    fallback: { "tty": false }
  },
  entry: './src/App.jsx',
  devtool: 'source-map',
  output: {
    filename: 'static/js/[name].[contenthash:5].js',
    chunkFilename: 'static/js/[chunkhash:4][name].[contenthash:5].js',
    publicPath: '/',
    path: path.resolve(__dirname, './dist/client'),
  },
  devServer: {
    historyApiFallback: true,
  },
  cache: {
    type: 'filesystem',
    buildDependencies: {
      config: [__filename],
    }
  },
  optimization: {
    minimizer: [
      new ESBuildMinifyPlugin()
    ],
    splitChunks: {
      chunks: 'all',
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        loader: 'esbuild-loader',
        options: {
          loader: 'jsx',
        },
      },
      {
        test: /\.tsx?$/,
        loader: 'esbuild-loader',
        include: path.resolve(__dirname, 'src'),
        exclude: /node_modules/,
        options: {
          loader: 'tsx',
          target: 'es2017',
        },
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
    {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
    ],
  },
  plugins: [
    new ForkTsCheckerWebpackPlugin(), 
    new HtmlWebpackPlugin({
      template: '!!raw-loader!./static/template.ejs',
      filename: './index.ejs',
    }),
    new HtmlWebpackPlugin({
      template: './static/template.ejs',
      filename: './index.html',
      templateParameters: {
        title: 'Knockout Forums',
        description: 'A welcoming gaming and lifestyle community!',
        type: 'website',
        url: 'https://knockout.chat/',
        image: 'https://knockout.chat/static/logo.png',
        date: new Date().toISOString(),
        schema: '',
        author: '',
        twitterCard: false,
        fediverseUserLink: '',
        icon: appConfig.favicon,
      },
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
    }),
    new webpack.DefinePlugin({
      'process.appConfig': JSON.stringify(appConfig),
    }),
  ],
};
